# kvase

Kvase is a 2D game(engine?) where the player faces waves of enemies and need to fend them off with whatever weapons they can find.  
The project is used as a basic introduction to the Vulkan API.  
It is written in C11 and currently runs only on Windows although the plan is to also support linux in the future.  
The game is still in the early prototype stage which means a lot of missing features and polish.

#### Current features include:
 - Sprite animation
 - AABB collision
 - Enemy spawning
 - Simple path finding
 - Player movement and attack
 - Particle effect for enemy death

#### TODO:
 - Implement gui support(most likely with [Nuklear](https://github.com/Immediate-Mode-UI/Nuklear) or something simpler like [microui](https://github.com/rxi/microui) by rxi)
 - A better animation system where animations are configured with external files
 - File system handling
 - Improved path finding
 - Multiple enemy types
 - Multiple playable characters
 - More weapons
 - Weapons with durability
 - A more populated and dynamic environment
 - Levels
# Preview
Placeholder art by [RGS_Dev](https://rgsdev.itch.io/animated-top-down-character-base-template-in-pixel-art-rgsdev)  

![Demo](https://wintrytbs.gitlab.io/website_test/images/kvase/example.gif)

# Dependencies
While developing the game the goal is to keep the dependencies to a minimum. All dependencies are included in the project:
 - `vk_platform`, `vulkan.h`, `vulkan_core.h`, `vulkan_win32.h` [From KhronosGroup](https://github.com/KhronosGroup/Vulkan-Headers/tree/main/include/vulkan)
 - `stb_image.h` [From Sean T. Barrett's(nothings) stb library](https://github.com/nothings/stb)
# Building and Running
 -  Run the `build.bat` file located in the **code** directory with **clang** available on the path.  
    This will produce a directory called **build** in the project root directory with an executable named `win32_kvase.exe`.  
 -  Run `win32_kvase_exe`. Currently the executable needs to be run from the build directory as a file system handler isn't  
    implemented yet and therefore all needed files are hardcoded with a relative path(relative to the build directory).

