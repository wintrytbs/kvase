#ifndef KVASE_TYPES_H
#define KVASE_TYPES_H

typedef int8_t  i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float  f32;
typedef double f64;

#define array_count(arr) (sizeof(arr) / sizeof(arr[0]))

// TODO: Move to a more appropriate place?
typedef struct FileData
{
    char *data;
    size_t size;
} FileData;

// TODO: Move to a more appropriate place
typedef struct
{
    u32 width;
    u32 height;
    u32 orig_channel_count;
    u32 channel_count;
    size_t size;
    unsigned char *data;
} kvase_ImageData;


#endif // KVASE_TYPES_H
