#include "kvase_vulkan.h"

#define GetDefaultCmd(ctx, id) Internal_GetDefaultCmd(ctx, DefaultCmdType_##id)
static inline VkCommandBuffer Internal_GetDefaultCmd(VulkanContext *ctx, DefaultCmdType cmd_id)
{
    VkCommandBuffer result = ctx->default_cmd_buffers[cmd_id];
    return result;
}

#define GetDefaultSemaphore(ctx, id) Internal_GetDefaultSemaphore(ctx, DefaultSemaphores_##id)
static inline VkSemaphore Internal_GetDefaultSemaphore(VulkanContext *ctx, DefaultSemaphores sem_id)
{
    VkSemaphore result = ctx->default_semaphores[sem_id];
    return result;
}


static VKAPI_ATTR VkBool32 VKAPI_CALL VulkanDebugReportCallbackProc(VkDebugReportFlagsEXT flags,
                                                                    VkDebugReportObjectTypeEXT object_type,
                                                                    uint64_t object,
                                                                    size_t location,
                                                                    int32_t message_code,
                                                                    const char *layer_prefix,
                                                                    const char *message,
                                                                    void *user_data)
{
    printf("VULKAN_DEBUG_REPORT_MESSAGE::\n%s\n", message);
    return VK_FALSE;
}

static VkFence kvase_vkCreateFence(VkDevice device)
{
    VkFence result = VK_NULL_HANDLE;
    VkFenceCreateInfo ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
    KVASE_VK_CHECK(vkCreateFence(device, &ci, NULL, &result));
    // TODO: handle errors
    return result;
}

// TODO: Uses platform specific functions
static VkShaderModule kvase_vkCreateShaderFromFile(VkDevice device_handle, const char *filename)
{
    FileData src = debug_win32_ReadFile(filename);

    assert(src.size % 4 == 0);

    VkShaderModuleCreateInfo shader_module_ci =
    {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = src.size,
        .pCode = (u32 *)src.data,
    };

    VkShaderModule shader_module;
    KVASE_VK_CHECK(vkCreateShaderModule(device_handle, &shader_module_ci, NULL, &shader_module));

    debug_win32_FreeFileData(&src);

    return shader_module;
}


// TODO: Parts of this is win32 specific
static bool InitBaseVulkan(HMODULE lib, HINSTANCE app_instance, HWND window_handle, VulkanContext *vulkan_ctx)
{
    bool result = false;
    vkGetInstanceProcAddr = (PFN_vkGetInstanceProcAddr)GetProcAddress(lib, "vkGetInstanceProcAddr");
    if(vkGetInstanceProcAddr)
    {
        printf("Successfully loaded function vkGetInstanceProcAddress\n");
        Vulkan_LoadGlobalFunctions();
        uint32_t version = 0;
        KVASE_VK_CHECK(vkEnumerateInstanceVersion(&version));
        printf("Major version: %u\n", VK_API_VERSION_MAJOR(version));
        printf("Minor version: %u\n", VK_API_VERSION_MINOR(version));
        printf("Patch version: %u\n", VK_API_VERSION_PATCH(version));
        printf("Variant version: %u\n", VK_API_VERSION_VARIANT(version));

        u32 ext_count = 0;
        KVASE_VK_CHECK(vkEnumerateInstanceExtensionProperties(NULL, &ext_count, NULL));
        printf("Extension count: %u\n", ext_count);
        assert(ext_count > 0);
        VkExtensionProperties *ext_properties = malloc(sizeof(VkExtensionProperties) * ext_count);
        KVASE_VK_CHECK(vkEnumerateInstanceExtensionProperties(NULL, &ext_count, ext_properties));
        u32 prop_count = 0;
        KVASE_VK_CHECK(vkEnumerateInstanceLayerProperties(&prop_count, NULL));
        assert(prop_count > 0);
        printf("\nProp count: %u\n", prop_count);
        VkLayerProperties *layer_properties = malloc(sizeof(VkLayerProperties) * prop_count);
        KVASE_VK_CHECK(vkEnumerateInstanceLayerProperties(&prop_count, layer_properties));
        assert(prop_count > 0);

        const char *wanted_extensions[] =
        {
            VK_KHR_SURFACE_EXTENSION_NAME,
            VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
            VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
        };

        u32 match_count = 0;
        for(u32 a = 0; a < array_count(wanted_extensions); a++)
        {
            for(u32 b = 0; b < ext_count; b++)
            {
                if(StringCompareZ(wanted_extensions[a], ext_properties[b].extensionName))
                {
                    match_count++;
                    break;
                }
            }
        }

        assert(match_count == array_count(wanted_extensions));

        const char *wanted_layers[] =
        {
            "VK_LAYER_KHRONOS_validation",
        };

        match_count = 0;
        for(u32 a = 0; a < array_count(wanted_layers); a++)
        {
            for(u32 b = 0; b < prop_count; b++)
            {
                if(StringCompareZ(wanted_layers[a], layer_properties[b].layerName))
                {
                    match_count++;
                    break;
                }
            }
        }

        assert(match_count == array_count(wanted_layers));

        VkApplicationInfo app_info =
        {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .pApplicationName = "Kvase Engine",
            .applicationVersion = 1u,
            .apiVersion = version,
        };

        VkInstanceCreateInfo instance_ci =
        {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &app_info,
            .enabledLayerCount = array_count(wanted_layers),
            .ppEnabledLayerNames = wanted_layers,
            .enabledExtensionCount = array_count(wanted_extensions),
            .ppEnabledExtensionNames = wanted_extensions,
        };

        VkInstance instance_handle;
        KVASE_VK_CHECK(vkCreateInstance(&instance_ci, NULL, &instance_handle));

        Vulkan_LoadInstanceFunctions(instance_handle);

        VkDebugReportCallbackCreateInfoEXT debug_callback_ci =
        {
            .sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT,
            .flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT | VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT,
            .pfnCallback = &VulkanDebugReportCallbackProc,
        };

        VkDebugReportCallbackEXT debug_callback;
        KVASE_VK_CHECK(vkCreateDebugReportCallbackEXT(instance_handle, &debug_callback_ci, NULL, &debug_callback));

        VkWin32SurfaceCreateInfoKHR surface_ci =
        {
            .sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR,
            .hinstance = app_instance,
            .hwnd = window_handle,
        };

        VkSurfaceKHR surface;
        KVASE_VK_CHECK(vkCreateWin32SurfaceKHR(instance_handle, &surface_ci, NULL, &surface));

        u32 device_count = 0;
        KVASE_VK_CHECK(vkEnumeratePhysicalDevices(instance_handle, &device_count, NULL));
        assert(device_count > 0);
        VkPhysicalDevice *physical_devices = malloc(sizeof(VkPhysicalDevice) * device_count);
        KVASE_VK_CHECK(vkEnumeratePhysicalDevices(instance_handle, &device_count, physical_devices));
        assert(device_count > 0);

        VkPhysicalDevice gpu_handle = NULL;
        u32 queue_family_index = 0;
        for(u32 di = 0; di < device_count; di++)
        {
            VkPhysicalDevice tmp_dev = physical_devices[di];
            u32 fam_count = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(tmp_dev, &fam_count, NULL);
            assert(fam_count > 0);
            VkQueueFamilyProperties *fam_properties = malloc(sizeof(VkQueueFamilyProperties) * fam_count);
            vkGetPhysicalDeviceQueueFamilyProperties(tmp_dev, &fam_count, fam_properties);
            assert(fam_count > 0);
            for(u32 fi = 0; fi < fam_count; fi++)
            {
                if(fam_properties[fi].queueFlags & (VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT))
                {
                    VkBool32 support;
                    KVASE_VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(tmp_dev, fi, surface, &support));
                    if(support)
                    {
                        queue_family_index = fi;
                        gpu_handle = tmp_dev;
                        break;
                    }
                }
            }

            free(fam_properties);
            if(gpu_handle)
            {
                break;
            }
        }

        assert(gpu_handle != NULL);


        f32 queue_priority = 1.0f;
        VkDeviceQueueCreateInfo device_queue_ci =
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
            .queueFamilyIndex = queue_family_index,
            .queueCount = 1,
            .pQueuePriorities = &queue_priority,
        };

        const char *wanted_device_extensions[] =
        {
            VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        };

        u32 device_ext_count;
        KVASE_VK_CHECK(vkEnumerateDeviceExtensionProperties(gpu_handle, NULL, &device_ext_count, NULL));
        assert(device_ext_count > 0);
        VkExtensionProperties *device_extensions = malloc(sizeof(VkExtensionProperties) * device_ext_count);
        KVASE_VK_CHECK(vkEnumerateDeviceExtensionProperties(gpu_handle, NULL, &device_ext_count, device_extensions));
        assert(device_ext_count > 0);

        match_count = 0;
        for(u32 a = 0; a < array_count(wanted_device_extensions); a++)
        {
            for(u32 b = 0; b < device_ext_count; b++)
            {
                if(StringCompareZ(wanted_device_extensions[a], device_extensions[b].extensionName))
                {
                    match_count++;
                }
            }
        }

        assert(match_count == array_count(wanted_device_extensions));

        VkDeviceCreateInfo device_ci =
        {
            .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
            .queueCreateInfoCount = 1,
            .pQueueCreateInfos = &device_queue_ci,
            .enabledExtensionCount = array_count(wanted_device_extensions),
            .ppEnabledExtensionNames = wanted_device_extensions,
            .pEnabledFeatures = NULL,
        };

        VkDevice device_handle;
        KVASE_VK_CHECK(vkCreateDevice(gpu_handle, &device_ci, NULL, &device_handle));

        Vulkan_LoadDeviceFunctions(device_handle);

        VkQueue device_queue;
        vkGetDeviceQueue(device_handle, queue_family_index, 0, &device_queue);

        vulkan_ctx->instance_handle = instance_handle;
        vulkan_ctx->surface_handle = surface;
        vulkan_ctx->gpu_handle = gpu_handle;
        vulkan_ctx->device_handle = device_handle;
        vulkan_ctx->device_queue_handle = device_queue;
        vulkan_ctx->queue_family_index = queue_family_index;

        vkGetPhysicalDeviceMemoryProperties(gpu_handle, &vulkan_ctx->gpu_mem_props);

        result = true;
    }

    return result;
}

static bool CreateAndInitSwapchain(ApplicationContext *app_ctx, VulkanContext *vulkan_ctx, u32 win_width, u32 win_height)
{
    VkSurfaceCapabilitiesKHR surface_capabilities;
    KVASE_VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(vulkan_ctx->gpu_handle, vulkan_ctx->surface_handle, &surface_capabilities));

    assert(surface_capabilities.supportedTransforms & VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR);
    assert(surface_capabilities.currentTransform == VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR);
    assert(surface_capabilities.maxImageCount >= 2 || surface_capabilities.maxImageCount == 0);
    assert(surface_capabilities.currentExtent.width > 0 || surface_capabilities.currentExtent.height > 0);
    printf("Current extent: %u, %u\n", surface_capabilities.currentExtent.width, surface_capabilities.currentExtent.height);
    printf("Min extent: %u, %u\n", surface_capabilities.minImageExtent.width, surface_capabilities.minImageExtent.height);
    printf("Max extent: %u, %u\n", surface_capabilities.maxImageExtent.width, surface_capabilities.maxImageExtent.height);


    VkExtent2D wanted_extent = {.width = win_width, .height = win_height};

    // NOTE: Handle when window is minimized
    assert(wanted_extent.width > 0 && wanted_extent.height > 0);
    // TODO: Just for testing
    assert(surface_capabilities.currentExtent.width == win_width &&
           surface_capabilities.currentExtent.height == win_height);
    if(surface_capabilities.currentExtent.width != 0xffffffff)
    {
        wanted_extent = surface_capabilities.currentExtent;
    }

    u32 format_count = 0;
    KVASE_VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan_ctx->gpu_handle, vulkan_ctx->surface_handle, &format_count, NULL));
    assert(format_count > 0);
    VkSurfaceFormatKHR *surface_formats = malloc(sizeof(VkSurfaceFormatKHR) * format_count);
    KVASE_VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(vulkan_ctx->gpu_handle, vulkan_ctx->surface_handle, &format_count, surface_formats));
    assert(format_count > 0);

    u32 format_index = UINT32_MAX;
    for(u32 i = 0; i < format_count; i++)
    {
        if(surface_formats[i].format == VK_FORMAT_B8G8R8A8_UNORM && surface_formats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
        {
            format_index = i;
        }
    }

    // TODO: handle error bad format
    assert(format_index != UINT32_MAX);

    VkSwapchainCreateInfoKHR swapchain_ci =
    {
        .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
        .surface = vulkan_ctx->surface_handle,
        .minImageCount = 2,
        .imageFormat = surface_formats[format_index].format,
        .imageColorSpace = surface_formats[format_index].colorSpace,
        .imageExtent = wanted_extent,
        .imageArrayLayers = 1,
        .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .preTransform = surface_capabilities.currentTransform,
        .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
        // TODO: Test different modes here to see differences
        // We then need to check which are supported by
        // calling vkGetPhysicalDeviceSurfacePresentModesKHR
        .presentMode = VK_PRESENT_MODE_FIFO_KHR,
        .clipped = VK_TRUE,
        .oldSwapchain = VK_NULL_HANDLE,
    };

    VkSwapchainKHR swapchain_handle;
    KVASE_VK_CHECK(vkCreateSwapchainKHR(vulkan_ctx->device_handle, &swapchain_ci, NULL, &swapchain_handle));

    u32 image_count = 0;
    KVASE_VK_CHECK(vkGetSwapchainImagesKHR(vulkan_ctx->device_handle, swapchain_handle, &image_count, NULL));
    assert(image_count >= 2);
    VkImage *swapchain_images = malloc(sizeof(VkImage) * image_count);
    KVASE_VK_CHECK(vkGetSwapchainImagesKHR(vulkan_ctx->device_handle, swapchain_handle, &image_count, swapchain_images));
    assert(image_count >= 2);

    VkImageView *swapchain_image_views = malloc(sizeof(VkImageView) * image_count);

    VkFenceCreateInfo submit_fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
    VkFence submit_fence;
    KVASE_VK_CHECK(vkCreateFence(vulkan_ctx->device_handle, &submit_fence_ci, NULL, &submit_fence));

    VkCommandBuffer setup_cmd = GetDefaultCmd(vulkan_ctx, SwapchainSetup);
    VkCommandBufferBeginInfo setup_begin_info = {.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO, .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT};

    for(size_t pi = 0; pi < image_count; pi++)
    {

        KVASE_VK_CHECK(vkBeginCommandBuffer(setup_cmd, &setup_begin_info));

        VkImageMemoryBarrier image_init_barr =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            .srcAccessMask = 0,
            .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
            .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
            .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
            .image = swapchain_images[pi],
            .subresourceRange =
            {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        };

        vkCmdPipelineBarrier(setup_cmd,
                             VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                             VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                             0,
                             0, NULL,
                             0, NULL,
                             1, &image_init_barr);

        KVASE_VK_CHECK(vkEndCommandBuffer(setup_cmd));

        VkPipelineStageFlags wait_stage_mask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        VkSubmitInfo submit_info =
        {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .pWaitDstStageMask = &wait_stage_mask,
            .commandBufferCount = 1,
            .pCommandBuffers = &setup_cmd,
        };
        KVASE_VK_CHECK(vkQueueSubmit(vulkan_ctx->device_queue_handle, 1, &submit_info, submit_fence));

        KVASE_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &submit_fence, VK_TRUE, UINT64_MAX));
        KVASE_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &submit_fence));

        KVASE_VK_CHECK(vkResetCommandBuffer(setup_cmd, 0));

        VkImageViewCreateInfo image_view_ci =
        {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = swapchain_images[pi],
            .viewType = VK_IMAGE_VIEW_TYPE_2D,
            .format = surface_formats[0].format,
            .components =
            {
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
                VK_COMPONENT_SWIZZLE_IDENTITY,
            },
            .subresourceRange =
            {
                .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                .baseMipLevel = 0,
                .levelCount = 1,
                .baseArrayLayer = 0,
                .layerCount = 1,
            },
        };

        KVASE_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &image_view_ci, NULL, &swapchain_image_views[pi]));
    }

    vkDestroyFence(vulkan_ctx->device_handle, submit_fence, NULL);

    vulkan_ctx->surface_formats = surface_formats;
    vulkan_ctx->surface_format_count = format_count;
    vulkan_ctx->swapchain_images = swapchain_images;
    vulkan_ctx->swapchain_image_count = image_count;
    vulkan_ctx->swapchain_image_views = swapchain_image_views;
    vulkan_ctx->swapchain_extent = wanted_extent;
    vulkan_ctx->swapchain_handle = swapchain_handle;

    // TODO: handle errors
    return true;
}

static bool CreateAndInitDepthImageView(VulkanContext *vulkan_ctx, VkFormat depth_format)
{
    VkImageCreateInfo depth_image_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        // TODO(torgrim): Check that the surface actually support this format
        .format = depth_format,
        .extent = {.width = vulkan_ctx->swapchain_extent.width, .height = vulkan_ctx->swapchain_extent.height, .depth = 1},
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    VkImage depth_image;
    KVASE_VK_CHECK(vkCreateImage(vulkan_ctx->device_handle, &depth_image_ci, NULL, &depth_image));

    VkMemoryRequirements mem_req;
    vkGetImageMemoryRequirements(vulkan_ctx->device_handle, depth_image, &mem_req);

    VkMemoryPropertyFlags wanted_memory = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    _Static_assert(VK_MAX_MEMORY_TYPES == 32u, "VK_MAX_MEMORY_TYPES invalid value");
    u32 mem_index = UINT32_MAX;
    for(u32 mi = 0; mi < vulkan_ctx->gpu_mem_props.memoryTypeCount; mi++)
    {
        if(mem_req.memoryTypeBits & (1 << mi) && vulkan_ctx->gpu_mem_props.memoryTypes[mi].propertyFlags & wanted_memory)
        {
            mem_index = mi;
            break;
        }
    }

    assert(mem_index != UINT32_MAX);

    VkMemoryAllocateInfo mem_allocate_info =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = mem_req.size,
        .memoryTypeIndex = mem_index,
    };

    VkDeviceMemory image_memory;
    KVASE_VK_CHECK(vkAllocateMemory(vulkan_ctx->device_handle, &mem_allocate_info, NULL, &image_memory));
    KVASE_VK_CHECK(vkBindImageMemory(vulkan_ctx->device_handle, depth_image, image_memory, 0));


    VkFenceCreateInfo depth_submit_fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
    VkFence depth_submit_fence;
    KVASE_VK_CHECK(vkCreateFence(vulkan_ctx->device_handle, &depth_submit_fence_ci, NULL, &depth_submit_fence));

    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };

    VkCommandBuffer cmd_buffer = GetDefaultCmd(vulkan_ctx, DepthSetup);
    KVASE_VK_CHECK(vkBeginCommandBuffer(cmd_buffer, &cmd_begin_info));

    VkImageMemoryBarrier depth_trans_barrier =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = depth_image,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    vkCmdPipelineBarrier(cmd_buffer,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                         VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT | VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &depth_trans_barrier
                         );

    KVASE_VK_CHECK(vkEndCommandBuffer(cmd_buffer));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = NULL,
        .pWaitDstStageMask = NULL,
        .commandBufferCount = 1,
        .pCommandBuffers = &cmd_buffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = NULL,
    };

    KVASE_VK_CHECK(vkQueueSubmit(vulkan_ctx->device_queue_handle, 1, &submit_info, depth_submit_fence));

    KVASE_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &depth_submit_fence, VK_TRUE, UINT64_MAX));
    KVASE_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &depth_submit_fence));

    KVASE_VK_CHECK(vkResetCommandBuffer(cmd_buffer, 0));

    VkImageViewCreateInfo depth_view_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = depth_image,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = depth_format,
        .components =
        {
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
            VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    VkImageView depth_image_view;
    KVASE_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &depth_view_ci, NULL, &depth_image_view));

    vkDestroyFence(vulkan_ctx->device_handle, depth_submit_fence, NULL);

    vulkan_ctx->depth_image = depth_image;
    vulkan_ctx->depth_image_view = depth_image_view;
    vulkan_ctx->depth_image_mem = image_memory;

    // TODO: handle errors
    return true;
}

static kvase_vkPipeline CreateDefaultPipeline(VulkanContext *vulkan_ctx,
                                              VkShaderModule vert_shader_module, VkShaderModule frag_shader_module,
                                              VkDescriptorSetLayout *desc_layouts, u32 desc_layout_count,
                                              VkPushConstantRange *push_constants, u32 push_constant_count)
{
    VkPipelineShaderStageCreateInfo stages[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_VERTEX_BIT,
            .module = vert_shader_module,
            .pName = "main",
        },
        {
            .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
            .stage = VK_SHADER_STAGE_FRAGMENT_BIT,
            .module = frag_shader_module,
            .pName = "main",
        },
    };

    VkVertexInputBindingDescription vertex_binding_list[] =
    {
        {
            .binding = 0,
            .stride = sizeof(VertexAttribs),
            .inputRate = VK_VERTEX_INPUT_RATE_VERTEX,
        },
    };

    VkVertexInputAttributeDescription vertex_attrib_list[] =
    {
        {
            .location = 0,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
        },
        {
            .location = 1,
            .binding = 0,
            .format = VK_FORMAT_R32G32B32A32_SFLOAT,
            .offset = offsetof(VertexAttribs, col),
        },
        {
            .location = 2,
            .binding = 0,
            .format = VK_FORMAT_R32G32_SFLOAT,
            .offset = offsetof(VertexAttribs, tex),
        },
    };

    VkPipelineVertexInputStateCreateInfo vert_input_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .vertexBindingDescriptionCount = array_count(vertex_binding_list),
        .pVertexBindingDescriptions = vertex_binding_list,
        .vertexAttributeDescriptionCount = array_count(vertex_attrib_list),
        .pVertexAttributeDescriptions = vertex_attrib_list,
    };

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
        .topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        .primitiveRestartEnable = VK_FALSE,
    };

    VkPipelineViewportStateCreateInfo viewport_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
        .viewportCount = 1,
        .scissorCount = 1,
    };

    VkPipelineRasterizationStateCreateInfo rasterization_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
        .depthClampEnable = VK_FALSE,
        .rasterizerDiscardEnable = VK_FALSE,
        .polygonMode = VK_POLYGON_MODE_FILL,
        .cullMode = VK_CULL_MODE_NONE,
        .frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE,
        .depthBiasEnable = VK_FALSE,
        .lineWidth = 1.0f,
    };

    VkPipelineMultisampleStateCreateInfo multisample_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
        .rasterizationSamples = VK_SAMPLE_COUNT_1_BIT,
    };

    VkPipelineDepthStencilStateCreateInfo depth_stencil_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
        .depthTestEnable = VK_FALSE,
        .depthWriteEnable = VK_FALSE,
        .depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL,
    };

    VkPipelineColorBlendAttachmentState color_blend_attachment =
    {
        .blendEnable = VK_TRUE,
        .srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        .colorBlendOp = VK_BLEND_OP_ADD,
        .srcAlphaBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
        .dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA,
        .alphaBlendOp = VK_BLEND_OP_ADD,
        .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

    VkPipelineColorBlendStateCreateInfo color_blend_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
        .logicOpEnable = VK_FALSE,
        .logicOp = VK_LOGIC_OP_CLEAR,
        .attachmentCount = 1,
        .pAttachments = &color_blend_attachment,
        .blendConstants = {0.0f, 0.0f, 0.0f, 0.0f},
    };

    VkDynamicState dynamic_state_list[] =
    {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR,
    };

    VkPipelineDynamicStateCreateInfo dynamic_state_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
        .dynamicStateCount = array_count(dynamic_state_list),
        .pDynamicStates = dynamic_state_list,
    };


    assert((push_constants == NULL && push_constant_count == 0) || push_constant_count > 0);
    VkPipelineLayoutCreateInfo pipeline_layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .setLayoutCount = desc_layout_count,
        .pSetLayouts = desc_layouts,
        .pushConstantRangeCount = push_constant_count,
        .pPushConstantRanges = push_constants,
    };

    VkPipelineLayout pipeline_layout_handle;
    KVASE_VK_CHECK(vkCreatePipelineLayout(vulkan_ctx->device_handle, &pipeline_layout_ci, NULL, &pipeline_layout_handle));

    VkGraphicsPipelineCreateInfo graphics_pipeline_ci =
    {
        .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
        .stageCount = array_count(stages),
        .pStages = stages,
        .pVertexInputState = &vert_input_state_ci,
        .pInputAssemblyState = &input_assembly_state_ci,
        .pViewportState = &viewport_state_ci,
        .pRasterizationState = &rasterization_state_ci,
        .pMultisampleState = &multisample_state_ci,
        .pDepthStencilState = &depth_stencil_state_ci,
        .pColorBlendState = &color_blend_state_ci,
        .pDynamicState = &dynamic_state_ci,
        .layout = pipeline_layout_handle,
        .renderPass = vulkan_ctx->default_render_pass_handle,
    };

    VkPipeline graphics_pipeline_handle;
    KVASE_VK_CHECK(vkCreateGraphicsPipelines(vulkan_ctx->device_handle, VK_NULL_HANDLE, 1, &graphics_pipeline_ci, NULL, &graphics_pipeline_handle));

    // TODO: Error handling
    kvase_vkPipeline result = {.pipeline_handle = graphics_pipeline_handle, .layout_handle = pipeline_layout_handle};
    return result;
}

static bool CreateDefaultRenderPass(VulkanContext *vulkan_ctx, VkFormat depth_format)
{
    VkAttachmentDescription attachment_list[] =
    {
        {
            .format = vulkan_ctx->surface_formats[vulkan_ctx->swapchain_image_format_index].format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
            .initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        },
        {
            .format = depth_format,
            .samples = VK_SAMPLE_COUNT_1_BIT,
            .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
            .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
            .initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        },

    };

    VkAttachmentReference color_attachment_ref = {.attachment = 0, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL};
    VkAttachmentReference depth_attachment_ref = {.attachment = 1, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL};
    VkSubpassDescription subpass =
    {
        .colorAttachmentCount = 1,
        .pColorAttachments = &color_attachment_ref,
        .pDepthStencilAttachment = &depth_attachment_ref,
    };

    VkRenderPassCreateInfo render_pass_ci =
    {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
        .attachmentCount = array_count(attachment_list),
        .pAttachments = attachment_list,
        .subpassCount = 1,
        .pSubpasses = &subpass,
        .dependencyCount = 0,
        .pDependencies = NULL,
    };

    VkRenderPass render_pass_handle;
    vkCreateRenderPass(vulkan_ctx->device_handle, &render_pass_ci, NULL, &render_pass_handle);
    vulkan_ctx->default_render_pass_handle = render_pass_handle;

    // TODO: handle errors
    return true;
}

static bool CreateDefaultFramebuffers(VulkanContext *vulkan_ctx)
{
    assert(vulkan_ctx->framebuffers == NULL);
    assert(vulkan_ctx->framebuffer_count == 0);

    VkFramebuffer *framebuffers = malloc(sizeof(VkFramebuffer) * vulkan_ctx->swapchain_image_count);
    for(size_t fi = 0; fi < vulkan_ctx->swapchain_image_count; fi++)
    {
        VkImageView fb_attachments[] =
        {
            vulkan_ctx->swapchain_image_views[fi],
            vulkan_ctx->depth_image_view,
        };
        VkFramebufferCreateInfo framebuffer_ci =
        {
            .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
            .renderPass = vulkan_ctx->default_render_pass_handle,
            .attachmentCount = 2,
            .pAttachments = fb_attachments,
            .width = vulkan_ctx->swapchain_extent.width,
            .height = vulkan_ctx->swapchain_extent.height,
            .layers = 1,
        };

        vkCreateFramebuffer(vulkan_ctx->device_handle, &framebuffer_ci, NULL, &framebuffers[fi]);
    }

    vulkan_ctx->framebuffers = framebuffers;
    vulkan_ctx->framebuffer_count = vulkan_ctx->swapchain_image_count;

    // TODO: handle errors
    return true;
}

static void DestroyPipeline(VkDevice device_handle, kvase_vkPipeline *pipeline)
{
    vkDeviceWaitIdle(device_handle);
    if(pipeline->layout_handle != VK_NULL_HANDLE)
    {
        vkDestroyPipelineLayout(device_handle, pipeline->layout_handle, NULL);
        pipeline->layout_handle = VK_NULL_HANDLE;
    }
    if(pipeline->pipeline_handle != VK_NULL_HANDLE)
    {
        vkDestroyPipeline(device_handle, pipeline->pipeline_handle, NULL);
        pipeline->pipeline_handle = VK_NULL_HANDLE;
    }
}

static void DestroySwapchainResources(VulkanContext *vulkan_ctx)
{
    if(vulkan_ctx->framebuffers != NULL)
    {
        for(u32 i = 0; i < vulkan_ctx->framebuffer_count; i++)
        {
            vkDestroyFramebuffer(vulkan_ctx->device_handle, vulkan_ctx->framebuffers[i], NULL);
            vulkan_ctx->framebuffers[i] = VK_NULL_HANDLE;
        }

        free(vulkan_ctx->framebuffers);
        vulkan_ctx->framebuffers = 0;
        vulkan_ctx->framebuffer_count = 0;
    }

    // TODO(torgrim): Handle images better
    if(vulkan_ctx->depth_image_view != VK_NULL_HANDLE)
    {
        assert(vulkan_ctx->depth_image != VK_NULL_HANDLE);
        assert(vulkan_ctx->depth_image_mem != VK_NULL_HANDLE);
        vkDestroyImageView(vulkan_ctx->device_handle, vulkan_ctx->depth_image_view, NULL);
        vkFreeMemory(vulkan_ctx->device_handle, vulkan_ctx->depth_image_mem, NULL);
        vkDestroyImage(vulkan_ctx->device_handle, vulkan_ctx->depth_image, NULL);
    }

    if(vulkan_ctx->swapchain_handle != VK_NULL_HANDLE)
    {
        assert(vulkan_ctx->swapchain_image_count > 0);
        for(u32 i = 0; i < vulkan_ctx->swapchain_image_count; i++)
        {
            vkDestroyImageView(vulkan_ctx->device_handle, vulkan_ctx->swapchain_image_views[i], NULL);
        }

        vkDestroySwapchainKHR(vulkan_ctx->device_handle, vulkan_ctx->swapchain_handle, NULL);

        free(vulkan_ctx->swapchain_images);
        free(vulkan_ctx->swapchain_image_views);

        vulkan_ctx->swapchain_images = NULL;
        vulkan_ctx->swapchain_image_views = NULL;
        vulkan_ctx->swapchain_image_count = 0;
    }

    if(vulkan_ctx->surface_format_count > 0)
    {
        free(vulkan_ctx->surface_formats);
        vulkan_ctx->surface_formats = NULL;
        vulkan_ctx->surface_format_count = 0;
    }
}

static VkDeviceMemory kvase_vkAllocateAndBindBufferMemory(VulkanContext *vulkan_ctx, VkBuffer buffer_handle, VkMemoryPropertyFlags wanted_mem_type)
{
    VkMemoryRequirements buffer_mem_req;
    vkGetBufferMemoryRequirements(vulkan_ctx->device_handle, buffer_handle, &buffer_mem_req);

    _Static_assert(VK_MAX_MEMORY_TYPES == 32u, "VK_MAX_MEMORY_TYPES invalid value");
    u32 mem_index = UINT32_MAX;
    for(u32 mi = 0; mi < vulkan_ctx->gpu_mem_props.memoryTypeCount; mi++)
    {
        if(buffer_mem_req.memoryTypeBits & (1 << mi) && vulkan_ctx->gpu_mem_props.memoryTypes[mi].propertyFlags & wanted_mem_type)
        {
            mem_index = mi;
            break;
        }
    }

    assert(mem_index != UINT32_MAX);

    VkMemoryAllocateInfo alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = buffer_mem_req.size,
        .memoryTypeIndex = mem_index,
    };

    VkDeviceMemory buffer_memory;
    KVASE_VK_CHECK(vkAllocateMemory(vulkan_ctx->device_handle, &alloc_info, NULL, &buffer_memory));
    KVASE_VK_CHECK(vkBindBufferMemory(vulkan_ctx->device_handle, buffer_handle, buffer_memory, 0));

    return buffer_memory;
}

static VkDeviceMemory kvase_vkAllocateAndBindImageMemory(VulkanContext *vulkan_ctx, VkImage image_handle, VkMemoryPropertyFlags wanted_mem_type)
{
    VkMemoryRequirements image_mem_req;
    vkGetImageMemoryRequirements(vulkan_ctx->device_handle, image_handle, &image_mem_req);

    _Static_assert(VK_MAX_MEMORY_TYPES == 32u, "VK_MAX_MEMORY_TYPES invalid value");
    u32 mem_index = UINT32_MAX;
    for(u32 mi = 0; mi < vulkan_ctx->gpu_mem_props.memoryTypeCount; mi++)
    {
        if(image_mem_req.memoryTypeBits & (1 << mi) && vulkan_ctx->gpu_mem_props.memoryTypes[mi].propertyFlags & wanted_mem_type)
        {
            mem_index = mi;
            break;
        }
    }

    assert(mem_index != UINT32_MAX);

    VkMemoryAllocateInfo alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
        .allocationSize = image_mem_req.size,
        .memoryTypeIndex = mem_index,
    };

    VkDeviceMemory image_memory;
    KVASE_VK_CHECK(vkAllocateMemory(vulkan_ctx->device_handle, &alloc_info, NULL, &image_memory));
    KVASE_VK_CHECK(vkBindImageMemory(vulkan_ctx->device_handle, image_handle, image_memory, 0));

    return image_memory;
}

static kvase_vkImage CreateTextureArray2D(VulkanContext *vulkan_ctx, kvase_ImageData *image_data, u32 image_count)
{
    VkImageCreateInfo image_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .extent =
        {
            .width = image_data->width,
            .height = image_data->height,
            .depth = 1,
        },
        .mipLevels = 1,
        .arrayLayers = image_count,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkImage image_handle;
    KVASE_VK_CHECK(vkCreateImage(vulkan_ctx->device_handle, &image_ci, NULL, &image_handle));

    VkDeviceMemory image_mem = kvase_vkAllocateAndBindImageMemory(vulkan_ctx, image_handle, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkImageViewCreateInfo image_view_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image_handle,
        .viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY,
        .format = image_ci.format,
        .components =
        {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = image_count,
        },
    };

    VkImageView image_view_handle;
    KVASE_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &image_view_ci, NULL, &image_view_handle));

    VkBufferCreateInfo staging_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = image_data->size * image_count,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer staging_buffer_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &staging_buffer_ci, NULL, &staging_buffer_handle));

    VkDeviceMemory staging_buffer_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, staging_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, staging_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));
    char *dst = mapped_mem;
    size_t offset = 0;
    for(u32 i = 0; i < image_count; i++)
    {
        memcpy(dst + offset, image_data[i].data, image_data[i].size);
        offset += image_data[i].size;
    }

    VkMappedMemoryRange mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = staging_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    KVASE_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &mem_range));

    VkCommandBuffer copy_cmd = GetDefaultCmd(vulkan_ctx, CopyBuffer);
    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };
    KVASE_VK_CHECK(vkBeginCommandBuffer(copy_cmd, &cmd_begin_info));

    VkImageMemoryBarrier from_undef_to_trans_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = image_count,
        },
    };
    vkCmdPipelineBarrier(copy_cmd,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_undef_to_trans_barr);



    VkBufferImageCopy copy_info =
    {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = image_count,
        },
        .imageExtent =
        {
            .width = image_data->width,
            .height = image_data->height,
            .depth = 1,
        },
    };
    vkCmdCopyBufferToImage(copy_cmd, staging_buffer_handle, image_handle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_info);

    VkImageMemoryBarrier from_trans_to_shader_read_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = image_count,
        },
    };

    vkCmdPipelineBarrier(copy_cmd,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_trans_to_shader_read_barr);

    KVASE_VK_CHECK(vkEndCommandBuffer(copy_cmd));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &copy_cmd,
    };

    VkFence fence = kvase_vkCreateFence(vulkan_ctx->device_handle);
    KVASE_VK_CHECK(vkQueueSubmit(vulkan_ctx->device_queue_handle, 1, &submit_info, fence));

    KVASE_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &fence, VK_TRUE, UINT64_MAX));
    KVASE_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &fence));
    vkDestroyFence(vulkan_ctx->device_handle, fence, NULL);

    vkDestroyBuffer(vulkan_ctx->device_handle, staging_buffer_handle, NULL);
    vkFreeMemory(vulkan_ctx->device_handle, staging_buffer_mem, NULL);

    kvase_vkImage result = {.image_handle = image_handle, .memory_handle = image_mem, .image_view_handle = image_view_handle};
    return result;
}

static kvase_vkImage CreateTextureDefault2D(VulkanContext *vulkan_ctx, kvase_ImageData *image_data)
{
    VkImageCreateInfo image_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_R8G8B8A8_UNORM,
        .extent =
        {
            .width = image_data->width,
            .height = image_data->height,
            .depth = 1,
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkImage image_handle;
    KVASE_VK_CHECK(vkCreateImage(vulkan_ctx->device_handle, &image_ci, NULL, &image_handle));

    VkDeviceMemory image_mem = kvase_vkAllocateAndBindImageMemory(vulkan_ctx, image_handle, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkImageViewCreateInfo image_view_ci =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = image_handle,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = image_ci.format,
        .components =
        {
            .r = VK_COMPONENT_SWIZZLE_IDENTITY,
            .g = VK_COMPONENT_SWIZZLE_IDENTITY,
            .b = VK_COMPONENT_SWIZZLE_IDENTITY,
            .a = VK_COMPONENT_SWIZZLE_IDENTITY,
        },
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    VkImageView image_view_handle;
    KVASE_VK_CHECK(vkCreateImageView(vulkan_ctx->device_handle, &image_view_ci, NULL, &image_view_handle));

    VkBufferCreateInfo staging_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = image_data->size,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer staging_buffer_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &staging_buffer_ci, NULL, &staging_buffer_handle));

    VkDeviceMemory staging_buffer_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, staging_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, staging_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));
    memcpy(mapped_mem, image_data->data, image_data->size);

    VkMappedMemoryRange mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = staging_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    KVASE_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &mem_range));

    VkCommandBuffer copy_cmd = GetDefaultCmd(vulkan_ctx, CopyBuffer);
    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };
    KVASE_VK_CHECK(vkBeginCommandBuffer(copy_cmd, &cmd_begin_info));

    VkImageMemoryBarrier from_undef_to_trans_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };
    vkCmdPipelineBarrier(copy_cmd,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_undef_to_trans_barr);



    VkBufferImageCopy copy_info =
    {
        .bufferOffset = 0,
        .bufferRowLength = 0,
        .bufferImageHeight = 0,
        .imageSubresource =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .mipLevel = 0,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
        .imageExtent =
        {
            .width = image_data->width,
            .height = image_data->height,
            .depth = 1,
        },
    };
    vkCmdCopyBufferToImage(copy_cmd, staging_buffer_handle, image_handle, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy_info);

    VkImageMemoryBarrier from_trans_to_shader_read_barr =
    {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = image_handle,
        .subresourceRange =
        {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1,
        },
    };

    vkCmdPipelineBarrier(copy_cmd,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
                         0,
                         0, NULL,
                         0, NULL,
                         1, &from_trans_to_shader_read_barr);

    KVASE_VK_CHECK(vkEndCommandBuffer(copy_cmd));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &copy_cmd,
    };

    VkFence fence = kvase_vkCreateFence(vulkan_ctx->device_handle);
    KVASE_VK_CHECK(vkQueueSubmit(vulkan_ctx->device_queue_handle, 1, &submit_info, fence));

    KVASE_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &fence, VK_TRUE, UINT64_MAX));
    KVASE_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &fence));
    vkDestroyFence(vulkan_ctx->device_handle, fence, NULL);

    vkDestroyBuffer(vulkan_ctx->device_handle, staging_buffer_handle, NULL);
    vkFreeMemory(vulkan_ctx->device_handle, staging_buffer_mem, NULL);

    kvase_vkImage result = {.image_handle = image_handle, .memory_handle = image_mem, .image_view_handle = image_view_handle};
    return result;
}

static kvase_vkBuffer CreateDirectVertexBufferEmpty(VulkanContext *vulkan_ctx, size_t size)
{
    VkBufferCreateInfo vertex_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = size,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer vertex_buffer_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &vertex_buffer_ci, NULL, &vertex_buffer_handle));

    VkDeviceMemory vert_buffer_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, vertex_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mem_mapped;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, vert_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mem_mapped));

    // NOTE: make this optional?
    memset(mem_mapped, 0, size);

    VkMappedMemoryRange vert_mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = vert_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };
    KVASE_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &vert_mem_range));

    // TODO: handle errors
    kvase_vkBuffer result = {.buffer_handle = vertex_buffer_handle, .memory_handle = vert_buffer_mem, .mem_mapped = mem_mapped};
    return result;
}

static kvase_vkBuffer CreateDirectVertexBuffer(VulkanContext *vulkan_ctx, VertexAttribs *attrib_list, u32 attrib_count)
{
    VkBufferCreateInfo vertex_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(VertexAttribs) * attrib_count,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer vertex_buffer_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &vertex_buffer_ci, NULL, &vertex_buffer_handle));

    VkDeviceMemory vert_buffer_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, vertex_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, vert_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));

    memcpy(mapped_mem, attrib_list, sizeof(VertexAttribs) * attrib_count);

    VkMappedMemoryRange vert_mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = vert_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };
    KVASE_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &vert_mem_range));

    vkUnmapMemory(vulkan_ctx->device_handle, vert_buffer_mem);

    // TODO: handle errors
    kvase_vkBuffer result = {.buffer_handle = vertex_buffer_handle, .memory_handle = vert_buffer_mem};
    return result;
}

static kvase_vkBuffer CreateVertexBufferWithStaging(VulkanContext *vulkan_ctx, VertexAttribs *vert_attrib_list, u32 vert_attrib_count)
{
    VkBufferCreateInfo vert_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(VertexAttribs) * vert_attrib_count,
        .usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer vert_buffer_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &vert_buffer_ci, NULL, &vert_buffer_handle));

    VkDeviceMemory vert_buffer_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, vert_buffer_handle, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

    VkBufferCreateInfo staging_buffer_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = vert_buffer_ci.size,
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer staging_buffer_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &staging_buffer_ci, NULL, &staging_buffer_handle));

    VkDeviceMemory staging_buffer_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, staging_buffer_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);

    void *mapped_mem;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, staging_buffer_mem, 0, VK_WHOLE_SIZE, 0, &mapped_mem));
    memcpy(mapped_mem, vert_attrib_list, sizeof(VertexAttribs) * vert_attrib_count);

    VkMappedMemoryRange mem_range =
    {
        .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
        .memory = staging_buffer_mem,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    KVASE_VK_CHECK(vkFlushMappedMemoryRanges(vulkan_ctx->device_handle, 1, &mem_range));

    VkCommandBuffer copy_cmd = GetDefaultCmd(vulkan_ctx, CopyBuffer);
    VkCommandBufferBeginInfo cmd_begin_info =
    {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
    };
    KVASE_VK_CHECK(vkBeginCommandBuffer(copy_cmd, &cmd_begin_info));

    VkBufferCopy copy_info =
    {
        .srcOffset = 0,
        .dstOffset = 0,
        .size = staging_buffer_ci.size,
    };
    vkCmdCopyBuffer(copy_cmd, staging_buffer_handle, vert_buffer_handle, 1, &copy_info);

    VkBufferMemoryBarrier copy_barrier =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
        .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .buffer = vert_buffer_handle,
        .offset = 0,
        .size = VK_WHOLE_SIZE,
    };

    vkCmdPipelineBarrier(copy_cmd, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, 0, 0, NULL, 1, &copy_barrier, 0, NULL);

    KVASE_VK_CHECK(vkEndCommandBuffer(copy_cmd));

    VkSubmitInfo submit_info =
    {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .commandBufferCount = 1,
        .pCommandBuffers = &copy_cmd,
    };

    VkFence fence = kvase_vkCreateFence(vulkan_ctx->device_handle);
    KVASE_VK_CHECK(vkQueueSubmit(vulkan_ctx->device_queue_handle, 1, &submit_info, fence));

    KVASE_VK_CHECK(vkWaitForFences(vulkan_ctx->device_handle, 1, &fence, VK_TRUE, UINT64_MAX));
    KVASE_VK_CHECK(vkResetFences(vulkan_ctx->device_handle, 1, &fence));
    vkDestroyFence(vulkan_ctx->device_handle, fence, NULL);

    vkDestroyBuffer(vulkan_ctx->device_handle, staging_buffer_handle, NULL);
    vkFreeMemory(vulkan_ctx->device_handle, staging_buffer_mem, NULL);

    kvase_vkBuffer result = {.buffer_handle = vert_buffer_handle, .memory_handle = vert_buffer_mem};
    return result;
}

static VkSampler kvase_vkCreateDefaultSampler(VulkanContext *vulkan_ctx)
{
    VkSamplerCreateInfo sampler_ci =
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .minFilter = VK_FILTER_NEAREST,
        .magFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
    };
    VkSampler sampler_handle;
    KVASE_VK_CHECK(vkCreateSampler(vulkan_ctx->device_handle, &sampler_ci, NULL, &sampler_handle));

    return sampler_handle;
}

static void kvase_UpdateDefaultTextureDescriptor(VulkanContext *vulkan_ctx, VkImageView image_view_handle, VkSampler sampler_handle, VkDescriptorSet desc_set)
{
    VkDescriptorImageInfo desc_image_infos[] =
    {
        {
            .sampler = sampler_handle,
            .imageView = image_view_handle,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        },
    };

    VkWriteDescriptorSet descriptor_writes[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = desc_set,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = array_count(desc_image_infos),
            .pImageInfo = desc_image_infos,
        },
    };

    vkUpdateDescriptorSets(vulkan_ctx->device_handle,
                           array_count(descriptor_writes), descriptor_writes,
                           0, NULL);
}

static kvase_vkDescriptorSetArray kvase_CreateDefaultTextureDescriptorArray(VulkanContext *vulkan_ctx)
{
    VkDescriptorPoolSize desc_pool_sizes[] =
    {
        {
            .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
        },
    };

    // TODO: We presumable want to create a common descriptor pool per pipeline?
    VkDescriptorPoolCreateInfo desc_pool_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = 1000,
        .poolSizeCount = array_count(desc_pool_sizes),
        .pPoolSizes = desc_pool_sizes,
    };
    VkDescriptorPool default_desc_pool;
    KVASE_VK_CHECK(vkCreateDescriptorPool(vulkan_ctx->device_handle, &desc_pool_ci, NULL, &default_desc_pool));

    VkDescriptorSetLayoutBinding desc_set_layout_binding[] =
    {
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
        },
    };

    VkDescriptorSetLayoutCreateInfo desc_set_layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = array_count(desc_set_layout_binding),
        .pBindings = desc_set_layout_binding,
    };

    VkDescriptorSetLayout desc_set_layout;
    KVASE_VK_CHECK(vkCreateDescriptorSetLayout(vulkan_ctx->device_handle, &desc_set_layout_ci, NULL, &desc_set_layout));

    VkDescriptorSet *desc_sets = calloc(1000, sizeof(VkDescriptorSet));
    for(u32 i = 0; i < 1000; i++)
    {
        VkDescriptorSetAllocateInfo desc_alloc_info =
        {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            .descriptorPool = default_desc_pool,
            .descriptorSetCount = 1,
            .pSetLayouts = &desc_set_layout,
        };

        KVASE_VK_CHECK(vkAllocateDescriptorSets(vulkan_ctx->device_handle, &desc_alloc_info, &desc_sets[i]));
    }
    kvase_vkDescriptorSetArray result = {0};
    result.layout_handle = desc_set_layout;
    result.set_handles = desc_sets;
    return result;
}

static kvase_vkPipelineDescriptor kvase_CreateDefaultUniformBufferDescriptor(VulkanContext *vulkan_ctx, SpritePipelineUBO *sprite_pipe_ubo)
{
    VkDescriptorPoolSize desc_pool_sizes[] =
    {
        {
            .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
        },
    };

    VkDescriptorPoolCreateInfo desc_pool_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = 1,
        .poolSizeCount = array_count(desc_pool_sizes),
        .pPoolSizes = desc_pool_sizes,
    };

    VkDescriptorPool default_desc_pool;
    KVASE_VK_CHECK(vkCreateDescriptorPool(vulkan_ctx->device_handle, &desc_pool_ci, NULL, &default_desc_pool));

    VkDescriptorSetLayoutBinding desc_set_layout_binding[] =
    {
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        },
    };

    VkDescriptorSetLayoutCreateInfo desc_set_layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = array_count(desc_set_layout_binding),
        .pBindings = desc_set_layout_binding,
    };

    VkDescriptorSetLayout desc_set_layout;
    KVASE_VK_CHECK(vkCreateDescriptorSetLayout(vulkan_ctx->device_handle, &desc_set_layout_ci, NULL, &desc_set_layout));

    VkDescriptorSetAllocateInfo desc_alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = default_desc_pool,
        .descriptorSetCount = 1,
        .pSetLayouts = &desc_set_layout,
    };
    VkDescriptorSet desc_set;
    KVASE_VK_CHECK(vkAllocateDescriptorSets(vulkan_ctx->device_handle, &desc_alloc_info, &desc_set));

    // TODO: Handle default buffer creation
    VkBufferCreateInfo ubo_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(SpritePipelineUBO),
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer ubo_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &ubo_ci, NULL, &ubo_handle));
    VkDeviceMemory ubo_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, ubo_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
    void *ubo_mapped = NULL;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, ubo_mem, 0, VK_WHOLE_SIZE, 0, &ubo_mapped));
    memcpy(ubo_mapped, sprite_pipe_ubo, sizeof(SpritePipelineUBO));

    VkDescriptorBufferInfo desc_buffer_infos[] =
    {
        {
            .buffer = ubo_handle,
            .offset = 0,
            .range = VK_WHOLE_SIZE,
        },
    };
    VkWriteDescriptorSet descriptor_writes[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = desc_set,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = array_count(desc_buffer_infos),
            .pBufferInfo = desc_buffer_infos,
        },
    };

    vkUpdateDescriptorSets(vulkan_ctx->device_handle,
                           array_count(descriptor_writes), descriptor_writes,
                           0, NULL);


    kvase_vkPipelineDescriptor result = {.set_handle = desc_set, .layout_handle = desc_set_layout};
    return result;
}

static kvase_vkPipelineDescriptor kvase_CreateSpritePipelineDescriptor(VulkanContext *vulkan_ctx, VkImageView image_view_handle, SpritePipelineUBO *sprite_pipe_ubo)
{
    VkSamplerCreateInfo sampler_ci =
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .minFilter = VK_FILTER_NEAREST,
        .magFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
    };
    VkSampler sampler_handle;
    KVASE_VK_CHECK(vkCreateSampler(vulkan_ctx->device_handle, &sampler_ci, NULL, &sampler_handle));

    VkDescriptorPoolSize desc_pool_sizes[] =
    {
        {
            .type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
        },
        {
            .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
        },
    };

    VkDescriptorPoolCreateInfo desc_pool_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = 1,
        .poolSizeCount = array_count(desc_pool_sizes),
        .pPoolSizes = desc_pool_sizes,
    };
    VkDescriptorPool default_desc_pool;
    KVASE_VK_CHECK(vkCreateDescriptorPool(vulkan_ctx->device_handle, &desc_pool_ci, NULL, &default_desc_pool));

    VkDescriptorSetLayoutBinding desc_set_layout_binding[] =
    {
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
        },
        {
            .binding = 1,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        },
    };

    VkDescriptorSetLayoutCreateInfo desc_set_layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = array_count(desc_set_layout_binding),
        .pBindings = desc_set_layout_binding,
    };

    VkDescriptorSetLayout desc_set_layout;
    KVASE_VK_CHECK(vkCreateDescriptorSetLayout(vulkan_ctx->device_handle, &desc_set_layout_ci, NULL, &desc_set_layout));

    VkDescriptorSetAllocateInfo desc_alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = default_desc_pool,
        .descriptorSetCount = 1,
        .pSetLayouts = &desc_set_layout,
    };
    VkDescriptorSet desc_set;
    KVASE_VK_CHECK(vkAllocateDescriptorSets(vulkan_ctx->device_handle, &desc_alloc_info, &desc_set));

    VkDescriptorImageInfo desc_image_infos[] =
    {
        {
            .sampler = sampler_handle,
            .imageView = image_view_handle,
            .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        },
    };

    // TODO: Handle default buffer creation
    VkBufferCreateInfo ubo_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(SpritePipelineUBO),
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer ubo_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &ubo_ci, NULL, &ubo_handle));
    VkDeviceMemory ubo_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, ubo_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
    void *ubo_mapped = NULL;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, ubo_mem, 0, VK_WHOLE_SIZE, 0, &ubo_mapped));
    memcpy(ubo_mapped, sprite_pipe_ubo, sizeof(SpritePipelineUBO));

    VkDescriptorBufferInfo desc_buffer_infos[] =
    {
        {
            .buffer = ubo_handle,
            .offset = 0,
            .range = VK_WHOLE_SIZE,
        },
    };
    VkWriteDescriptorSet descriptor_writes[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = desc_set,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
            .descriptorCount = array_count(desc_image_infos),
            .pImageInfo = desc_image_infos,
        },
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = desc_set,
            .dstBinding = 1,
            .dstArrayElement = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = array_count(desc_buffer_infos),
            .pBufferInfo = desc_buffer_infos,
        },
    };

    vkUpdateDescriptorSets(vulkan_ctx->device_handle,
                           array_count(descriptor_writes), descriptor_writes,
                           0, NULL);


    kvase_vkPipelineDescriptor result = {.set_handle = desc_set, .layout_handle = desc_set_layout};
    return result;
}

static kvase_vkPipelineDescriptor kvase_CreateDebugPipelineDescriptor(VulkanContext *vulkan_ctx, DebugPipelineUBO *ubo)
{
    VkSamplerCreateInfo sampler_ci =
    {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .minFilter = VK_FILTER_NEAREST,
        .magFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
    };

    VkDescriptorPoolSize desc_pool_sizes[] =
    {
        {
            .type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
        },
    };

    VkDescriptorPoolCreateInfo desc_pool_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .maxSets = 1,
        .poolSizeCount = array_count(desc_pool_sizes),
        .pPoolSizes = desc_pool_sizes,
    };
    VkDescriptorPool default_desc_pool;
    KVASE_VK_CHECK(vkCreateDescriptorPool(vulkan_ctx->device_handle, &desc_pool_ci, NULL, &default_desc_pool));

    VkDescriptorSetLayoutBinding desc_set_layout_binding[] =
    {
        {
            .binding = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = 1,
            .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
        },
    };

    VkDescriptorSetLayoutCreateInfo desc_set_layout_ci =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .bindingCount = array_count(desc_set_layout_binding),
        .pBindings = desc_set_layout_binding,
    };

    VkDescriptorSetLayout desc_set_layout;
    KVASE_VK_CHECK(vkCreateDescriptorSetLayout(vulkan_ctx->device_handle, &desc_set_layout_ci, NULL, &desc_set_layout));

    VkDescriptorSetAllocateInfo desc_alloc_info =
    {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .descriptorPool = default_desc_pool,
        .descriptorSetCount = 1,
        .pSetLayouts = &desc_set_layout,
    };
    VkDescriptorSet desc_set;
    KVASE_VK_CHECK(vkAllocateDescriptorSets(vulkan_ctx->device_handle, &desc_alloc_info, &desc_set));

    // TODO: Handle default buffer creation
    VkBufferCreateInfo ubo_ci =
    {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .size = sizeof(DebugPipelineUBO),
        .usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    };

    VkBuffer ubo_handle;
    KVASE_VK_CHECK(vkCreateBuffer(vulkan_ctx->device_handle, &ubo_ci, NULL, &ubo_handle));
    VkDeviceMemory ubo_mem = kvase_vkAllocateAndBindBufferMemory(vulkan_ctx, ubo_handle, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT);
    void *ubo_mapped = NULL;
    KVASE_VK_CHECK(vkMapMemory(vulkan_ctx->device_handle, ubo_mem, 0, VK_WHOLE_SIZE, 0, &ubo_mapped));
    memcpy(ubo_mapped, ubo, sizeof(DebugPipelineUBO));

    VkDescriptorBufferInfo desc_buffer_infos[] =
    {
        {
            .buffer = ubo_handle,
            .offset = 0,
            .range = VK_WHOLE_SIZE,
        },
    };

    VkWriteDescriptorSet descriptor_writes[] =
    {
        {
            .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
            .dstSet = desc_set,
            .dstBinding = 0,
            .dstArrayElement = 0,
            .descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
            .descriptorCount = array_count(desc_buffer_infos),
            .pBufferInfo = desc_buffer_infos,
        },
    };

    vkUpdateDescriptorSets(vulkan_ctx->device_handle,
                           array_count(descriptor_writes), descriptor_writes,
                           0, NULL);


    kvase_vkPipelineDescriptor result = {.set_handle = desc_set, .layout_handle = desc_set_layout};
    return result;
}

