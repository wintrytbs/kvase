#ifndef KVASE_MATH_H
#define KVASE_MATH_H

#include <math.h>

#define PI_F32 3.141592f

typedef union Vec2
{
    struct
    {
        f32 x;
        f32 y;
    };

    struct
    {
        f32 width;
        f32 height;
    };
} Vec2;

typedef union Vec2I
{
    struct
    {
        i32 x;
        i32 y;
    };

    struct
    {
        i32 width;
        i32 height;
    };
} Vec2I;

typedef union Vec2U
{
    struct
    {
        u32 x;
        u32 y;
    };

    struct
    {
        u32 width;
        u32 height;
    };
} Vec2U;

typedef union Vec4
{
    struct
    {
        f32 x;
        f32 y;
        f32 z;
        f32 w;
    };

    struct
    {
        f32 r;
        f32 g;
        f32 b;
        f32 a;
    };
} Vec4;

static inline f32 DegreeToRadianF32(f32 deg)
{
    f32 result = (deg * (PI_F32/180.0f));
    return result;
}

static inline f32 SquareRootF32(f32 s)
{
    f32 result = sqrtf(s);
    return result;
}

static inline f32 CosRadF32(f32 rad)
{
    f32 result = cosf(rad);
    return result;
}

static inline f32 CosDegF32(f32 deg)
{
    f32 result = cosf(DegreeToRadianF32(deg));
    return result;
}

static inline f32 AbsF32(f32 v)
{
    f32 result = fabsf(v);
    return result;
}

static inline Vec2 vec2_Abs(Vec2 a)
{
    Vec2 result = {.x = AbsF32(a.x), .y = AbsF32(a.y)};
    return result;
}

static inline Vec2 vec2_Add(Vec2 a, Vec2 b)
{
    Vec2 result = {.x = a.x + b.x, .y = a.y + b.y};
    return result;
}

static inline Vec2 vec2_Sub(Vec2 a, Vec2 b)
{
    Vec2 result = {.x = a.x - b.x, .y = a.y - b.y};
    return result;
}

static inline Vec2 vec2_MulF32(Vec2 a, f32 s)
{
    Vec2 result = {.x = a.x * s, .y = a.y * s};
    return result;
}

static inline Vec2 vec2_Mul(Vec2 a, Vec2 b)
{
    Vec2 result = {.x = a.x * b.x, .y = a.y * b.y};
    return result;
}

static inline Vec2 vec2_DivF32(Vec2 a, f32 s)
{
    assert(s != 0.0f);
    Vec2 result = {.x = a.x / s, .y = a.y / s};
    return result;
}

static inline f32 vec2_LengthSq(Vec2 a)
{
    f32 result = a.x * a.x + a.y * a.y;
    return result;
}

static inline f32 vec2_Length(Vec2 a)
{
    f32 result = SquareRootF32(vec2_LengthSq(a));
    return result;
}

static inline f32 vec2_Distance(Vec2 a, Vec2 b)
{
    f32 result = vec2_Length(vec2_Sub(a, b));
    return result;
}

static inline Vec2 vec2_Normalize(Vec2 a)
{
    f32 l = vec2_Length(a);
    Vec2 result = vec2_DivF32(a, l);
    return result;
}

static inline f32 vec2_Dot(Vec2 a, Vec2 b)
{
    f32 result = a.x * b.x + a.y * b.y;
    return result;
}

static inline bool vec2_Equal(Vec2 a, Vec2 b)
{
    bool result = (a.x == b.x && a.y == b.y);
    return result;
}

static inline bool v2u_Equal(Vec2U a, Vec2U b)
{
    bool result = (a.x == b.x && a.y == b.y);
    return result;
}



#endif // KVASE_MATH_H

