#ifndef WIN32_KVASE_H
#define WIN32_KVASE_H

typedef struct ApplicationContext
{
    HINSTANCE app_instance;
    HWND window_handle;
} ApplicationContext;


static FileData debug_win32_ReadFile(const char *filename);
static void debug_win32_FreeFileData(FileData *file_data);

// TODO: Put this into a more appropriate place
static inline bool StringCompareZ(const char *s1, const char *s2)
{
    bool result = (strcmp(s1, s2) == 0);
    return result;
}

#endif // WIN32_KVASE_H
