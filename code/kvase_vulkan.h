#ifndef KVASE_VULKAN_H
#define KVASE_VULKAN_H

// TODO: These are not really specific
// to vulkan so should probably be moved
// to a more general place.
typedef struct VertexAttribs
{
    Vec4 pos;
    Vec4 col;
    Vec2 tex;
} VertexAttribs;
_Static_assert(sizeof(VertexAttribs) == 40, "VertexAttribs unexpected size");

typedef struct
{
    VertexAttribs *data;
    u32 count;
    u32 capacity;
    size_t unit_size;
} kvase_VertexBuffer;

typedef struct
{
    f32 proj[16];
} SpritePipelineUBO;

typedef struct
{
    f32 proj[16];
} DebugPipelineUBO;

// END TODO: move

typedef enum DefaultCmdType
{
    DefaultCmdType_SwapchainSetup,
    DefaultCmdType_DepthSetup,
    DefaultCmdType_Render,
    DefaultCmdType_CopyBuffer,

    DefaultCmdType_COUNT,
} DefaultCmdType;

typedef enum DefaultSemaphores
{
    DefaultSemaphores_RenderDone,
    DefaultSemaphores_PresentDone,

    DefaultSemaphores_COUNT,
} DefaultSemaphores;

typedef struct VulkanContext
{
    // NOTE: base items
    VkInstance instance_handle;
    VkSurfaceKHR surface_handle;
    VkPhysicalDevice gpu_handle;
    VkPhysicalDeviceMemoryProperties gpu_mem_props;
    VkDevice device_handle;
    VkQueue device_queue_handle;
    u32 queue_family_index;

    // NOTE: swapchain related
    VkCommandBuffer default_cmd_buffers[DefaultCmdType_COUNT];
    VkImage *swapchain_images;
    VkImageView *swapchain_image_views;
    u32 swapchain_image_count;
    VkSurfaceFormatKHR *surface_formats;
    u32 surface_format_count;
    // TODO: just store the format and color space
    // directly?
    u32 swapchain_image_format_index;
    VkExtent2D swapchain_extent;
    VkSwapchainKHR swapchain_handle;

    // NOTE: render pass related
    VkRenderPass default_render_pass_handle;
    // NOTE: framebuffer related?
    VkFramebuffer *framebuffers;
    u32 framebuffer_count;

    VkImage depth_image;
    VkImageView depth_image_view;
    VkDeviceMemory depth_image_mem;

    // NOTE: semaphore and fence handling
    VkSemaphore default_semaphores[DefaultSemaphores_COUNT];

} VulkanContext;

typedef struct
{
    VkBuffer buffer_handle;
    VkDeviceMemory memory_handle;
    void *mem_mapped;
} kvase_vkBuffer;

typedef struct
{
    VkImage image_handle;
    VkDeviceMemory memory_handle;
    VkImageView image_view_handle;
} kvase_vkImage;

typedef struct
{
    VkPipeline pipeline_handle;
    VkPipelineLayout layout_handle;
} kvase_vkPipeline;

typedef struct
{
    VkDescriptorSet set_handle;
    VkDescriptorSetLayout layout_handle;
} kvase_vkPipelineDescriptor;

typedef struct
{
    VkDescriptorSet *set_handles;
    VkDescriptorSetLayout layout_handle;
} kvase_vkDescriptorSetArray;

static inline void KVASE_VK_CHECK(VkResult result)
{
    if(result != VK_SUCCESS)
    {
        assert(false);
    }
}

#endif // KVASE_VULKAN_H
