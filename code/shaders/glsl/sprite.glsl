#version 450 core

#if defined(VERT_SHADER)
layout (location = 0) in vec4 in_pos;
layout (location = 1) in vec4 in_col;
layout (location = 2) in vec2 in_tex_coord;

layout (location = 0) out vec4 col;
layout (location = 1) out vec2 tex_coord;

layout (set = 0, binding = 0) uniform mat_ubo
{
    mat4 proj;
};

layout (push_constant) uniform pc
{
    vec2 s;
    vec2 t;
    vec2 tile_coord;
};

void main()
{
    gl_Position = proj * vec4(in_pos.x * s.x + t.x, in_pos.y * s.y + t.y, in_pos.zw);
    col = in_col;
    tex_coord = in_tex_coord;

}
#elif defined(FRAG_SHADER)
layout (location = 0) out vec4 out_color;

layout (location = 0) in vec4 col;
layout (location = 1) in vec2 tex_coord;
layout (set = 1, binding = 0) uniform sampler2D u_tex;

void main()
{
    out_color = texture(u_tex, tex_coord);
}
#else
error
#endif
