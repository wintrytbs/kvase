#version 450 core

#if defined(VERT_SHADER)

layout(location = 0) in vec4 in_pos;
layout(location = 1) in vec4 in_col;
layout(location = 2) in vec2 in_tex;

layout(location = 0) out vec4 out_col;

layout(binding = 0) uniform mat_ubo
{
    mat4 proj;
};

void main()
{
    gl_Position = proj * vec4(in_pos.xy, 0.0f, 1.0f);
    out_col = in_col;
}

#elif defined(FRAG_SHADER)

layout(location = 0) in vec4 in_col;

layout(location = 0) out vec4 out_col;

void main()
{
    out_col = in_col;
}

#else
error
#endif
