#include <stdio.h>
#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#define VK_NO_PROTOTYPES
#include "vulkan/include/vulkan.h"
#include "vulkan/include/vulkan_win32.h"

#if defined(__clang__)
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wcast-qual"
#pragma clang diagnostic ignored "-Wsign-conversion"
#pragma clang diagnostic ignored "-Wdisabled-macro-expansion"
#pragma clang diagnostic ignored "-Wdouble-promotion"
#pragma clang diagnostic ignored "-Wextra-semi-stmt"
#pragma clang diagnostic ignored "-Wimplicit-int-conversion"
#pragma clang diagnostic ignored "-Wmissing-prototypes"
#pragma clang diagnostic ignored "-Wimplicit-fallthrough"
#endif

#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"


#if defined(__clang__)
#pragma clang diagnostic pop
#endif


#include "tbs_vulkan_loader.h"

#include "kvase_types.h"
#include "kvase_math.h"

#include "win32_kvase.h"
#include "kvase_vulkan.c"

#include "kvase_asset_paths.h"

typedef struct
{
    Vec2 min;
    Vec2 max;
} kvase_Rect2D;

enum InputKey
{
    InputKey_W = 0,
    InputKey_S,
    InputKey_A,
    InputKey_D,
    InputKey_Space,
    InputKey_F,
    InputKey_T,
    InputKey_Tab,
    InputKey_COUNT,
};

typedef struct InputData
{
    bool down;
    f32 down_time;
} InputData;

typedef enum
{
    PlayerState_Idle,
    PlayerState_Walk,
    PlayerState_Attack,
    PlayerState_Roll,

    PlayerState_COUNT,
} PlayerState;

typedef enum
{
    PlayerType_WithoutWeapon,
    PlayerType_WithSwordAndShield,
    PlayerType_WithPistol,
    PlayerType_Dead,

    PlayerType_COUNT,
} PlayerType;

typedef enum
{
    PlayerDirection_Down,
    PlayerDirection_Left,
    PlayerDirection_Right,
    PlayerDirection_Up,

    PlayerDirection_COUNT,
} PlayerDirection;
typedef struct
{
    u32 handle;
} FrameInfo;
// TODO: Rename this
typedef struct AnimationData
{
    f32 speed;
    u32 required_loop_count;
    u32 frame_count;
    kvase_vkImage *frames;
} AnimationData;

// TODO: Rename this
typedef struct AnimationInfo
{
    AnimationData *spec;
    u32 frame_index;
    u32 loop_count;
    f32 time;
    // TODO: Used to not increment any data on the initial animation change.
    // Probably a better way to do this.
    bool playing;
} AnimationInfo;

typedef struct PlayerData
{
    Vec2 pos;
    Vec2 scale;
    // TODO: used for rolling to know which direction to continue the roll.
    // This could probably be done in a better way
    Vec2 prev_move_dir;

    PlayerDirection face_dir;
    // TODO: This is probably not needed. Currently used to check if the direction has changed.
    // This can most likely just be done locally.
    PlayerDirection prev_face_dir;

    PlayerType type;
    PlayerType prev_type;
    PlayerState state;
    PlayerState prev_state;


    AnimationData anim_without_weapon[PlayerState_COUNT][PlayerDirection_COUNT];
    AnimationData anim_with_sword_and_shield[PlayerState_COUNT][PlayerDirection_COUNT];
    AnimationData anim_with_pistol[PlayerState_COUNT][PlayerDirection_COUNT];
    AnimationData anim_dead;

    AnimationInfo current_anim;

} PlayerData;

static void UpdatePlayerCurrentAnimation(PlayerData *player)
{
    // NOTE: Is it clearer to just set the field directly to zero/false?
    memset(&player->current_anim, 0, sizeof(AnimationInfo));

    PlayerState state = player->state;
    PlayerDirection direction = player->face_dir;
    switch(player->type)
    {
        case PlayerType_WithoutWeapon:
        {
            player->current_anim.spec = &player->anim_without_weapon[state][direction];
        } break;
        case PlayerType_WithSwordAndShield:
        {
            player->current_anim.spec = &player->anim_with_sword_and_shield[state][direction];
        } break;
        case PlayerType_WithPistol:
        {
            player->current_anim.spec = &player->anim_with_pistol[state][direction];
        } break;
        case PlayerType_Dead:
        {
            player->current_anim.spec = &player->anim_dead;
        } break;
        default:
        {
            // NOTE: Invalid type
            assert(false);
        } break;
    }

    assert(player->current_anim.spec != NULL);
}

// TODO: Better name for this since it's called each frame
// but not necessarily changing someting each time it's called.
static void PlayPlayerAnimation(PlayerData *player, f32 dt)
{
    assert(player->current_anim.spec != NULL);

    if(player->current_anim.playing)
    {
        player->current_anim.time += dt;
        if(player->current_anim.time >= player->current_anim.spec->speed)
        {
            player->current_anim.time = 0.0f;
            if(player->current_anim.frame_index < player->current_anim.spec->frame_count-1)
            {
                player->current_anim.frame_index++;
            }
            else
            {
                player->current_anim.frame_index = 0;
                player->current_anim.loop_count++;
            }
        }
    }
    else
    {
        player->current_anim.playing = true;
    }

    assert(player->current_anim.frame_index < player->current_anim.spec->frame_count);
}

static AnimationData LoadAnimationData(VulkanContext *vulkan_ctx, kvase_AnimAsset *anim_asset)
{
    AnimationData result = {0};
    result.speed = anim_asset->speed;
    result.frame_count = anim_asset->frame_count;
    result.required_loop_count = anim_asset->required_loop_count;
    result.frames = calloc(result.frame_count, sizeof(kvase_vkImage));
    for(u32 i = 0; i < anim_asset->frame_count; i++)
    {
        int w;
        int h;
        int n;
        int c = 4;
        unsigned char *pixels = stbi_load(anim_asset->paths[i], &w, &h, &n, c);
        assert(pixels);

        kvase_ImageData image_data =
        {
            .width = (u32)w,
            .height = (u32)h,
            .orig_channel_count = (u32)n,
            .channel_count = (u32)c,
            .size = (size_t)(w * h * c) * sizeof(unsigned char),
            .data = pixels,
        };

        result.frames[i] = CreateTextureDefault2D(vulkan_ctx, &image_data);

        stbi_image_free(pixels);
    }
    return result;
}

static kvase_Rect2D TranslateRect2D(kvase_Rect2D rect, Vec2 trans)
{
    kvase_Rect2D result =
    {
        .min = vec2_Add(rect.min, trans),
        .max = vec2_Add(rect.max, trans)
    };

    return result;
}

static kvase_Rect2D ScaleRect2D(kvase_Rect2D rect, Vec2 scale)
{
    assert(false); // NOTE: Not implemented yet
    kvase_Rect2D result = {0};
    return result;
}

static kvase_Rect2D CreateRect2D(Vec2 pos, Vec2 size)
{
    Vec2 half_size = vec2_MulF32(size, 0.5f);
    kvase_Rect2D result =
    {
        .min = vec2_Sub(pos, half_size),
        .max = vec2_Add(pos, half_size),
    };

    return result;
}

static f32 GetRandom01F32(void)
{
    int v = rand();
    f64 norm = (f64)v / (f64)RAND_MAX;
    f32 result = (f32)norm;
    return result;
}

static f32 GetRandomBetweenF32(f32 min, f32 max)
{
    assert(min < max);

    f32 range = (max - min);
    f32 n = GetRandom01F32();
    f32 result = min + (n * range);
    return result;
}

typedef struct
{
    Vec2 pos;
    Vec2 vel;
    Vec4 col;
    Vec2 baseline;
    f32 bounce_count;
    f32 time_alive;
    f32 time_to_live;
} Debug_Particle;

typedef struct
{
    bool active;
    Debug_Particle particles[15];
    f32 time_alive;
    f32 time_to_live;
} Debug_ParticleEmitter;

typedef struct
{
    Debug_ParticleEmitter items[200];
} Debug_EmitterPool;

static Debug_ParticleEmitter *Debug_EmitterPool_FindFree(Debug_EmitterPool *pool)
{
    Debug_ParticleEmitter *result = NULL;

    for(u32 a = 0; a < array_count(pool->items); a++)
    {
        if(pool->items[a].active == false)
        {
            result = &pool->items[a];
            memset(result, 0, sizeof(Debug_ParticleEmitter));
        }
    }

    assert(result != NULL);
    return result;
}

typedef enum EnemyType
{
    EnemyType_Normal,

    EnemyType_COUNT,
} EnemyType;

typedef enum EnemyState
{
    EnemyState_None,
    EnemyState_Idle,
    EnemyState_Walk,

    EnemyState_COUNT,
} EnemyState;

// TODO: Ok for now, but probably want to split this
// up into different structures for easier data handling
typedef enum
{
    EnemyDirection_Down,
    EnemyDirection_Right,
    EnemyDirection_Left,
    EnemyDirection_Up,

    EnemyDirection_COUNT,
} EnemyDirection;

typedef struct
{
    AnimationData normal[EnemyState_COUNT][EnemyDirection_COUNT];
} kvase_EnemyAnimations;

typedef struct
{
    Vec2 pos;
    Vec2 scale;
    Vec2 target;

    EnemyType type;
    EnemyState state;
    EnemyState prev_state;
    EnemyDirection face_dir;
    EnemyDirection prev_face_dir;

    f32 target_update_timer;

    AnimationInfo current_anim;
} kvase_EnemyEntity;

// TODO: Just make this into a general
// fixed runtime array
typedef struct
{
    kvase_EnemyEntity *data;
    u32 capacity;
    u32 count;
} kvase_EnemyArray;

static void UpdateEnemyCurrentAnimation(kvase_EnemyEntity *enemy, kvase_EnemyAnimations *animations)
{
    assert(enemy->state != EnemyState_None);
    // NOTE: Is it clearer to just set the field directly to zero/false?
    memset(&enemy->current_anim, 0, sizeof(AnimationInfo));

    EnemyState state = enemy->state;
    EnemyDirection direction = enemy->face_dir;
    switch(enemy->type)
    {
        case EnemyType_Normal:
        {
            enemy->current_anim.spec = &animations->normal[state][direction];
        } break;
        default:
        {
            // NOTE: Invalid type
            assert(false);
        } break;
    }

    assert(enemy->current_anim.spec != NULL);
}

// TODO: Better name for this since it's called each frame
// but not necessarily changing someting each time it's called.
static void PlayEnemyAnimation(kvase_EnemyEntity *enemy, f32 dt)
{
    assert(enemy->current_anim.spec != NULL);

    if(enemy->current_anim.playing)
    {
        enemy->current_anim.time += dt;
        if(enemy->current_anim.time >= enemy->current_anim.spec->speed)
        {
            enemy->current_anim.time = 0.0f;
            if(enemy->current_anim.frame_index < enemy->current_anim.spec->frame_count-1)
            {
                enemy->current_anim.frame_index++;
            }
            else
            {
                enemy->current_anim.frame_index = 0;
                enemy->current_anim.loop_count++;
            }
        }
    }
    else
    {
        enemy->current_anim.playing = true;
    }

    assert(enemy->current_anim.frame_index < enemy->current_anim.spec->frame_count);
}

static kvase_EnemyEntity *SpawnEnemy(kvase_EnemyArray *list, Vec2 spawn_pos)
{
    assert(list->count < list->capacity);
    kvase_EnemyEntity *e = &list->data[list->count++];
    memset(e, 0, sizeof(kvase_EnemyEntity));
    e->pos = spawn_pos;
    e->scale = (Vec2){.width = 256.0f, .height = 256.0f};
    e->state = EnemyState_Idle;
    e->type = EnemyType_Normal;
    e->face_dir = EnemyDirection_Down;

    return e;
}

// TODO: use double precision?
static inline f32 win32_GetElapsedSeconds(LARGE_INTEGER start_time, LARGE_INTEGER perf_count_freq)
{
    LARGE_INTEGER end_time;
    QueryPerformanceCounter(&end_time);
    LARGE_INTEGER elapsed_ticks;
    elapsed_ticks.QuadPart = end_time.QuadPart - start_time.QuadPart;
    f32 result = (f32)((elapsed_ticks.QuadPart * 1000000) / perf_count_freq.QuadPart);
    result /= 1000000.0f;
    return result;
}
// NOTE: These sizes are client area so does not include border size etc
static inline Vec2 win32_GetWindowSizeF32(HWND window_handle)
{
    RECT rect;
    GetClientRect(window_handle, &rect);
    assert(rect.left == 0 && rect.top == 0);
    Vec2 result = {.width = (f32)rect.right, .height = (f32)rect.bottom};
    return result;
}

static inline Vec2U win32_GetWindowSizeU32(HWND window_handle)
{
    RECT rect;
    GetClientRect(window_handle, &rect);
    assert(rect.left == 0 && rect.top == 0);
    Vec2U result = {.width = (u32)rect.right, .height = (u32)rect.bottom};
    return result;
}

static FileData debug_win32_ReadFile(const char *filename)
{
    FileData result = {0};
    HANDLE file_handle = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,NULL);
    assert(file_handle != INVALID_HANDLE_VALUE);

    LARGE_INTEGER tmp_file_size;
    unsigned long file_size = 0;
    if(GetFileSizeEx(file_handle, &tmp_file_size))
    {
        assert(tmp_file_size.QuadPart < LONG_MAX);
        file_size = (unsigned long)tmp_file_size.QuadPart;
    }
    else
    {
        assert(false);
    }

    unsigned long bytes_read = 0;
    char *buffer = malloc(file_size);
    if(ReadFile(file_handle, buffer, file_size, &bytes_read, NULL))
    {
        result.data = buffer;
        result.size = bytes_read;
    }
    else
    {
        assert(false);
    }

    assert(file_size == bytes_read);

    CloseHandle(file_handle);

    return result;
}

static void debug_win32_FreeFileData(FileData *file_data)
{
    assert(file_data->size > 0);
    assert(file_data->data);

    free(file_data->data);
    file_data->data = NULL;
    file_data->size = 0;
}

static void kvase_AddRect(kvase_VertexBuffer *buffer, Vec2 min, Vec2 max, Vec4 col)
{
    assert(buffer->capacity > buffer->count + 24);

    const f32 thickness = 5.0f;
    Vec2 outer_min = min;
    Vec2 outer_max = max;
    Vec2 inner_min = {.x = min.x + thickness, .y = min.y + thickness};
    Vec2 inner_max = {.x = max.x - thickness, .y = max.y - thickness};

    // top
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_min.x, .y = inner_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_min.y}, .col = col, .tex = {0}};

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_max.x, .y = outer_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_min.y}, .col = col, .tex = {0}};

    // right
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_max.x, .y = outer_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_max.y}, .col = col, .tex = {0}};

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_max.x, .y = outer_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_max.x, .y = outer_min.y}, .col = col, .tex = {0}};

    // bottom
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_max.x, .y = outer_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_max.y}, .col = col, .tex = {0}};

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_max.x, .y = inner_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_min.x, .y = inner_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_max.y}, .col = col, .tex = {0}};

    // left
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_min.x, .y = inner_max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_min.x, .y = inner_min.y}, .col = col, .tex = {0}};

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = inner_min.x, .y = inner_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = outer_min.x, .y = outer_max.y}, .col = col, .tex = {0}};

}

static void kvase_AddRectFilled(kvase_VertexBuffer *buffer, Vec2 min, Vec2 max, Vec4 col)
{
    assert(buffer->capacity > buffer->count + 6);

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = min.x, .y = min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = max.x, .y = min.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = max.x, .y = max.y}, .col = col, .tex = {0}};

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = max.x, .y = max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = min.x, .y = max.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = min.x, .y = min.y}, .col = col, .tex = {0}};
}

static void kvase_AddLine(kvase_VertexBuffer *buffer, Vec2 start, Vec2 end, Vec4 col)
{
    assert(buffer->capacity > buffer->count + 6);
    // TODO: Solve this properly
    assert((end.x > start.x && end.y >= start.y) ||
           (end.x >= start.x && end.y > start.y));

    Vec2 normal = vec2_Sub(end, start);
    Vec2 unit_norm = vec2_Normalize(normal);
    Vec2 perp = {.x = -unit_norm.y, .y = unit_norm.x};
    Vec2 s0 = {.x = start.x + (-perp.x),            .y = start.y + (-perp.y)};
    Vec2 s1 = {.x = end.x +   (-perp.x) + normal.x, .y = end.y +   (-perp.y) + normal.y};
    Vec2 s2 = {.x = end.x +    perp.x + normal.x,   .y = end.y +     perp.y + normal.y};
    Vec2 s3 = {.x = end.x +    perp.x + normal.x,   .y = end.y +     perp.y + normal.y};
    Vec2 s4 = {.x = start.x +  perp.x,              .y = start.y +     perp.y};
    Vec2 s5 = {.x = start.x + (-perp.x),            .y = start.y +   (-perp.y)};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = s0.x, .y = s0.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = s1.x, .y = s1.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = s2.x, .y = s2.y}, .col = col, .tex = {0}};

    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = s3.x, .y = s3.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = s4.x, .y = s4.y}, .col = col, .tex = {0}};
    buffer->data[buffer->count++] = (VertexAttribs){.pos = {.x = s5.x, .y = s5.y}, .col = col, .tex = {0}};
}

static LRESULT CALLBACK MainWindowCallbackProc(HWND window_handle, UINT msg, WPARAM wParam, LPARAM lParam)
{
    LRESULT result = 0;
    switch(msg)
    {
        case WM_DESTROY:
        {
            PostQuitMessage(0);
        } break;
        default:
        {
            result = DefWindowProc(window_handle, msg, wParam, lParam);
        }
    }

    return result;
}

int main(void)
{
    srand(1);

    HMODULE app_instance = GetModuleHandleA(NULL);
    {
        // TODO: Temporary fix for handling file paths. Do a proper pass on paths/filesystem api.
        char binary_path[MAX_PATH] = {0};
        char binary_dir[MAX_PATH] = {0};
        DWORD result_length = GetModuleFileNameA(app_instance, binary_path, MAX_PATH);
        printf("Binary path: %s\n", binary_path);
        assert(result_length > 0);
        assert(GetLastError() == 0);
        size_t i = result_length-1;
        while(i > 0 && binary_path[i] != '\\')
        {
            i--;
        }
        assert(i > 0);
        memcpy(binary_dir, binary_path, i+1);
        printf("Binary dir: %s\n", binary_dir);
        if(!SetCurrentDirectory(binary_dir))
        {
            assert(false);
        }
    }

    WNDCLASSEXA win_class =
    {
        .cbSize = sizeof(WNDCLASSEXA),
        .lpfnWndProc = MainWindowCallbackProc,
        .hInstance = app_instance,
        .lpszClassName = "kvase_window_class",
        .style = CS_VREDRAW | CS_HREDRAW,
    };

    RegisterClassExA(&win_class);
    HWND window_handle = CreateWindowA(win_class.lpszClassName,
                                       "Kvase Engine",
                                       WS_VISIBLE | WS_OVERLAPPEDWINDOW,
                                       CW_USEDEFAULT,
                                       CW_USEDEFAULT,
                                       CW_USEDEFAULT,
                                       CW_USEDEFAULT,
                                       0,
                                       NULL,
                                       app_instance,
                                       NULL);


    HMODULE vulkan_lib = LoadLibrary("../libs/vulkan/bin/vulkan-1.dll");
    if(vulkan_lib)
    {
        printf("Successfully loaded vulkan library\n");
        // TODO: alloc both on heap?
        ApplicationContext app_ctx = {.app_instance = app_instance, .window_handle = window_handle};
        VulkanContext vulkan_ctx = {0};
        if(InitBaseVulkan(vulkan_lib, app_instance, window_handle, &vulkan_ctx))
        {
            VkCommandPoolCreateInfo cmd_pool_ci =
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
                .queueFamilyIndex = vulkan_ctx.queue_family_index,
            };

            VkCommandPool cmd_pool;
            KVASE_VK_CHECK(vkCreateCommandPool(vulkan_ctx.device_handle, &cmd_pool_ci, NULL, &cmd_pool));

            {
                VkCommandBufferAllocateInfo cmd_buffer_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };
                VkCommandBuffer cmd_buffer;
                KVASE_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &cmd_buffer_alloc_info, &cmd_buffer));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_DepthSetup] = cmd_buffer;
            }

            {
                VkCommandBufferAllocateInfo setup_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer setup_cmd;
                KVASE_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &setup_cmd_alloc_info, &setup_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_SwapchainSetup] = setup_cmd;
            }

            {
                VkCommandBufferAllocateInfo render_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer render_cmd;
                KVASE_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &render_cmd_alloc_info, &render_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_Render] = render_cmd;
            }

            {
                VkCommandBufferAllocateInfo copy_cmd_alloc_info =
                {
                    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    .commandPool = cmd_pool,
                    .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    .commandBufferCount = 1,
                };

                VkCommandBuffer copy_cmd;
                KVASE_VK_CHECK(vkAllocateCommandBuffers(vulkan_ctx.device_handle, &copy_cmd_alloc_info, &copy_cmd));
                vulkan_ctx.default_cmd_buffers[DefaultCmdType_CopyBuffer] = copy_cmd;
            }

            Vec2U window_size = win32_GetWindowSizeU32(app_ctx.window_handle);
            CreateAndInitSwapchain(&app_ctx, &vulkan_ctx, window_size.width, window_size.height);


            // TODO(torgrim): Here we actually need to check that this is supported
            // by the surface
            VkFormat default_depth_format = VK_FORMAT_D16_UNORM;
            CreateAndInitDepthImageView(&vulkan_ctx, default_depth_format);
            CreateDefaultRenderPass(&vulkan_ctx, default_depth_format);
            CreateDefaultFramebuffers(&vulkan_ctx);

            VkShaderModule sprite_pipe_vert_shader_module = kvase_vkCreateShaderFromFile(vulkan_ctx.device_handle, "../code/shaders/spirv/sprite_vert.spv");
            VkShaderModule sprite_pipe_frag_shader_module = kvase_vkCreateShaderFromFile(vulkan_ctx.device_handle, "../code/shaders/spirv/sprite_frag.spv");
            VkShaderModule debug_pipe_vert_shader_module = kvase_vkCreateShaderFromFile(vulkan_ctx.device_handle, "../code/shaders/spirv/debug_vert.spv");
            VkShaderModule debug_pipe_frag_shader_module = kvase_vkCreateShaderFromFile(vulkan_ctx.device_handle, "../code/shaders/spirv/debug_frag.spv");

            f32 tex_offset_x = 1.0f;
            f32 tex_offset_y = 1.0f;

            VertexAttribs vertex_data[] =
            {
                {.pos = {.x = -0.5f, .y =  0.5f, .w = 1.0f}, .col = {.r = 1.0f, .a = 1.0f},             .tex = {.x = 0.0f,         .y = tex_offset_y}},
                {.pos = {.x =  0.5f, .y =  0.5f, .w = 1.0f}, .col = {.g = 1.0f, .a = 1.0f},             .tex = {.x = tex_offset_x, .y = tex_offset_y}},
                {.pos = {.x =  0.5f, .y = -0.5f, .w = 1.0f}, .col = {.b = 1.0f, .a = 1.0f},             .tex = {.x = tex_offset_x, .y = 0.0f}},

                {.pos = {.x =  0.5f, .y = -0.5f, .w = 1.0f}, .col = {.b = 1.0f, .a = 1.0f},             .tex = {.x = tex_offset_x, .y = 0.0f}},
                {.pos = {.x = -0.5f, .y = -0.5f, .w = 1.0f}, .col = {.r = 1.0f, .b = 1.0f, .a = 1.0f},  .tex = {.x = 0.0f,         .y = 0.0f}},
                {.pos = {.x = -0.5f, .y =  0.5f, .w = 1.0f}, .col = {.r = 1.0f, .a = 1.0f},             .tex = {.x = 0.0f,         .y = tex_offset_y}},
            };

            // tbs_VulkanBuffer vertex_buffer = CreateDirectVertexBuffer(&vulkan_ctx, vertex_data, array_count(vertex_data));
            kvase_vkBuffer vertex_buffer = CreateVertexBufferWithStaging(&vulkan_ctx, vertex_data, array_count(vertex_data));

            kvase_VertexBuffer debug_vert_buffer = {0};
            debug_vert_buffer.capacity = 100000u;
            debug_vert_buffer.unit_size = sizeof(VertexAttribs);
            debug_vert_buffer.data = malloc(debug_vert_buffer.unit_size * debug_vert_buffer.capacity);
            printf("Debug buffer allocated size: %zu\n", debug_vert_buffer.capacity * debug_vert_buffer.unit_size);
            kvase_vkBuffer debug_pipe_vk_vert_buffer = CreateDirectVertexBufferEmpty(&vulkan_ctx, debug_vert_buffer.capacity * debug_vert_buffer.unit_size);

            Vec2 min = {.x = 300.0f, .y = -200.0f};
            Vec2 max = {.x = 400.0f, .y = 0.0f};
            Vec4 col = {.r = 1.0f, .g = 0.0f, .b = 1.0f, .a = 1.0f};
            kvase_AddRect(&debug_vert_buffer, min, max, col);
            min.x -= 300.0f;
            max.x -= 300.0f;
            kvase_AddRectFilled(&debug_vert_buffer, min, max, col);

            // NOTE: this will be done each frame since we could potentially update
            // the backing buffer each frame.
            assert(debug_vert_buffer.count > 0);
            memcpy(debug_pipe_vk_vert_buffer.mem_mapped, debug_vert_buffer.data, debug_vert_buffer.unit_size * debug_vert_buffer.count);

            kvase_VertexBuffer particle_vert_buffer = {0};
            particle_vert_buffer.capacity = 100000u;
            particle_vert_buffer.unit_size = sizeof(VertexAttribs);
            particle_vert_buffer.data = malloc(particle_vert_buffer.unit_size * particle_vert_buffer.capacity);
            printf("Particle buffer allocated size: %zu\n", particle_vert_buffer.capacity * particle_vert_buffer.unit_size);
            kvase_vkBuffer particle_pipe_vk_vert_buffer = CreateDirectVertexBufferEmpty(&vulkan_ctx, particle_vert_buffer.capacity * particle_vert_buffer.unit_size);


            {
                VkSemaphore present_done_sem;
                VkSemaphore render_done_sem;
                VkSemaphoreCreateInfo sem_ci = {.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO};
                vkCreateSemaphore(vulkan_ctx.device_handle, &sem_ci, NULL, &present_done_sem);
                vkCreateSemaphore(vulkan_ctx.device_handle, &sem_ci, NULL, &render_done_sem);

                vulkan_ctx.default_semaphores[DefaultSemaphores_PresentDone] = present_done_sem;
                vulkan_ctx.default_semaphores[DefaultSemaphores_RenderDone] = render_done_sem;
            }

            PlayerData *player = calloc(1, sizeof(PlayerData));
            player->scale.width = 256.0f;
            player->scale.height = 256.0f;
            player->face_dir = PlayerDirection_Down;
            player->prev_face_dir = player->face_dir;
            player->type = PlayerType_WithoutWeapon;
            player->prev_type = player->type;
            player->state = PlayerState_Idle;
            player->prev_state = player->state;

            {

                // TODO: When loading an asset we want the meta-data(speed, loop etc) to be specified in the same place as the paths
                // instead of directly in the function?
                player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_0);
                player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_1);
                player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_2);
                player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_3);

                player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_4);
                player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_5);
                player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_6);
                player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_7);

                player->anim_without_weapon[PlayerState_Attack][PlayerDirection_Down]   = LoadAnimationData(&vulkan_ctx, &anim_asset_8);
                player->anim_without_weapon[PlayerState_Attack][PlayerDirection_Right]  = LoadAnimationData(&vulkan_ctx, &anim_asset_9);
                player->anim_without_weapon[PlayerState_Attack][PlayerDirection_Left]   = LoadAnimationData(&vulkan_ctx, &anim_asset_10);
                player->anim_without_weapon[PlayerState_Attack][PlayerDirection_Up]     = LoadAnimationData(&vulkan_ctx, &anim_asset_11);

                player->anim_without_weapon[PlayerState_Roll][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_12);
                player->anim_without_weapon[PlayerState_Roll][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_13);
                player->anim_without_weapon[PlayerState_Roll][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_14);
                player->anim_without_weapon[PlayerState_Roll][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_15);


                player->anim_with_sword_and_shield[PlayerState_Idle][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_16);
                player->anim_with_sword_and_shield[PlayerState_Idle][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_17);
                player->anim_with_sword_and_shield[PlayerState_Idle][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_18);
                player->anim_with_sword_and_shield[PlayerState_Idle][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_19);

                player->anim_with_sword_and_shield[PlayerState_Walk][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_20);
                player->anim_with_sword_and_shield[PlayerState_Walk][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_21);
                player->anim_with_sword_and_shield[PlayerState_Walk][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_22);
                player->anim_with_sword_and_shield[PlayerState_Walk][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_23);

                player->anim_with_sword_and_shield[PlayerState_Attack][PlayerDirection_Down]   = LoadAnimationData(&vulkan_ctx, &anim_asset_24);
                player->anim_with_sword_and_shield[PlayerState_Attack][PlayerDirection_Left]   = LoadAnimationData(&vulkan_ctx, &anim_asset_25);
                player->anim_with_sword_and_shield[PlayerState_Attack][PlayerDirection_Right]  = LoadAnimationData(&vulkan_ctx, &anim_asset_26);
                player->anim_with_sword_and_shield[PlayerState_Attack][PlayerDirection_Up]     = LoadAnimationData(&vulkan_ctx, &anim_asset_27);

                player->anim_with_sword_and_shield[PlayerState_Roll][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_28);
                player->anim_with_sword_and_shield[PlayerState_Roll][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_29);
                player->anim_with_sword_and_shield[PlayerState_Roll][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_30);
                player->anim_with_sword_and_shield[PlayerState_Roll][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_31);


                player->anim_with_pistol[PlayerState_Idle][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_32);
                player->anim_with_pistol[PlayerState_Idle][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_33);
                player->anim_with_pistol[PlayerState_Idle][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_34);
                player->anim_with_pistol[PlayerState_Idle][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_35);

                player->anim_with_pistol[PlayerState_Walk][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_36);
                player->anim_with_pistol[PlayerState_Walk][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_37);
                player->anim_with_pistol[PlayerState_Walk][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_38);
                player->anim_with_pistol[PlayerState_Walk][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_39);

                player->anim_with_pistol[PlayerState_Attack][PlayerDirection_Down]   = LoadAnimationData(&vulkan_ctx, &anim_asset_40);
                player->anim_with_pistol[PlayerState_Attack][PlayerDirection_Left]   = LoadAnimationData(&vulkan_ctx, &anim_asset_41);
                player->anim_with_pistol[PlayerState_Attack][PlayerDirection_Right]  = LoadAnimationData(&vulkan_ctx, &anim_asset_42);
                player->anim_with_pistol[PlayerState_Attack][PlayerDirection_Up]     = LoadAnimationData(&vulkan_ctx, &anim_asset_43);

                player->anim_with_pistol[PlayerState_Roll][PlayerDirection_Down]     = LoadAnimationData(&vulkan_ctx, &anim_asset_44);
                player->anim_with_pistol[PlayerState_Roll][PlayerDirection_Left]     = LoadAnimationData(&vulkan_ctx, &anim_asset_45);
                player->anim_with_pistol[PlayerState_Roll][PlayerDirection_Right]    = LoadAnimationData(&vulkan_ctx, &anim_asset_46);
                player->anim_with_pistol[PlayerState_Roll][PlayerDirection_Up]       = LoadAnimationData(&vulkan_ctx, &anim_asset_47);

                player->anim_dead = LoadAnimationData(&vulkan_ctx, &anim_asset_100);

                UpdatePlayerCurrentAnimation(player);
            }

            kvase_EnemyAnimations *enemy_animations = calloc(1, sizeof(kvase_EnemyAnimations));
            {
                enemy_animations->normal[EnemyState_Idle][EnemyDirection_Down]  = player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Down];
                enemy_animations->normal[EnemyState_Idle][EnemyDirection_Right] = player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Right];
                enemy_animations->normal[EnemyState_Idle][EnemyDirection_Left]  = player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Left];
                enemy_animations->normal[EnemyState_Idle][EnemyDirection_Up]    = player->anim_without_weapon[PlayerState_Idle][PlayerDirection_Up];

                enemy_animations->normal[EnemyState_Walk][EnemyDirection_Down]  = player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Down];
                enemy_animations->normal[EnemyState_Walk][EnemyDirection_Right] = player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Right];
                enemy_animations->normal[EnemyState_Walk][EnemyDirection_Left]  = player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Left];
                enemy_animations->normal[EnemyState_Walk][EnemyDirection_Up]    = player->anim_without_weapon[PlayerState_Walk][PlayerDirection_Up];
            }

            VkSampler default_sprite_sampler_handle = kvase_vkCreateDefaultSampler(&vulkan_ctx);

            kvase_vkDescriptorSetArray sprite_pipe_texture_descs = {0};
            kvase_vkPipelineDescriptor sprite_pipe_ubo_desc = {0};
            {
                Vec2 win_size = win32_GetWindowSizeF32(app_ctx.window_handle);
                SpritePipelineUBO sprite_pipeline_ubo =
                {
                    {
                        2.0f / (win_size.width), 0.0f,                   0.0f,  0.0f,
                        0.0f,                    2.0f / win_size.height, 0.0f,  0.0f,
                        0.0f,                    0.0f,                   1.0f,  0.0f,
                        0.0f,                    0.0f,                   1.0f,  1.0f,
                    },
                };

                sprite_pipe_texture_descs = kvase_CreateDefaultTextureDescriptorArray(&vulkan_ctx);
                sprite_pipe_ubo_desc = kvase_CreateDefaultUniformBufferDescriptor(&vulkan_ctx, &sprite_pipeline_ubo);
            }

            kvase_vkPipelineDescriptor debug_pipe_desc = {0};
            {
                Vec2 win_size = win32_GetWindowSizeF32(app_ctx.window_handle);
                DebugPipelineUBO debug_pipeline_ubo =
                {
                    {
                        2.0f / (win_size.width), 0.0f,                   0.0f,  0.0f,
                        0.0f,                    2.0f / win_size.height, 0.0f,  0.0f,
                        0.0f,                    0.0f,                   1.0f,  0.0f,
                        0.0f,                    0.0f,                   1.0f,  1.0f,
                    },
                };
                debug_pipe_desc = kvase_CreateDebugPipelineDescriptor(&vulkan_ctx, &debug_pipeline_ubo);
            }

            VkPushConstantRange sprite_pipe_push_constant_configs[] =
            {
                {
                    .stageFlags = VK_SHADER_STAGE_VERTEX_BIT,
                    .offset = 0,
                    .size = sizeof(Vec2) * 2,
                },
            };

            VkDescriptorSetLayout sprite_pipe_desc_layouts[] =
            {
                sprite_pipe_ubo_desc.layout_handle,
                sprite_pipe_texture_descs.layout_handle,
            };

            kvase_vkPipeline sprite_pipeline = CreateDefaultPipeline(&vulkan_ctx,
                                                                     sprite_pipe_vert_shader_module, sprite_pipe_frag_shader_module,
                                                                     sprite_pipe_desc_layouts, array_count(sprite_pipe_desc_layouts),
                                                                     sprite_pipe_push_constant_configs, array_count(sprite_pipe_push_constant_configs));

#if 1
            kvase_vkPipeline debug_pipeline = CreateDefaultPipeline(&vulkan_ctx,
                                                                    debug_pipe_vert_shader_module, debug_pipe_frag_shader_module,
                                                                    &debug_pipe_desc.layout_handle, 1,
                                                                    NULL, 0);
#endif


            InputData input_key_map[InputKey_COUNT];
            for(size_t i = 0; i < array_count(input_key_map); i++)
            {
                input_key_map[i].down = false;
                input_key_map[i].down_time = -1.0f;
            }

            bool app_running = true;

            LARGE_INTEGER perf_count_freq;
            QueryPerformanceFrequency(&perf_count_freq);
            f32 dt = 1.0f / 60.0f;
            f32 runtime = 0.0f;
            f32 sprite_timeline = 0.0f;

            u64 frame_id = 0;

            typedef struct PlayerStateNode
            {
                bool connections[PlayerState_COUNT];
            } PlayerStateNode;
            typedef struct PlayerStateMachine
            {
                PlayerStateNode states[PlayerState_COUNT];
            } PlayerStateMachine;

            PlayerStateMachine player_state_machine[PlayerType_COUNT] = {0};
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Idle].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Idle].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Idle].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Idle].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Walk].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Walk].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Walk].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Walk].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Attack].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Attack].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Attack].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Attack].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Roll].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Roll].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Roll].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithoutWeapon].states[PlayerState_Roll].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Idle].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Idle].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Idle].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Idle].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Walk].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Walk].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Walk].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Walk].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Attack].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Attack].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Attack].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Attack].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Roll].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Roll].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Roll].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithSwordAndShield].states[PlayerState_Roll].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithPistol].states[PlayerState_Idle].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Idle].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Idle].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Idle].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithPistol].states[PlayerState_Walk].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Walk].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Walk].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Walk].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithPistol].states[PlayerState_Attack].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Attack].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Attack].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Attack].connections[PlayerState_Roll] = true;

            player_state_machine[PlayerType_WithPistol].states[PlayerState_Roll].connections[PlayerState_Idle] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Roll].connections[PlayerState_Walk] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Roll].connections[PlayerState_Attack] = true;
            player_state_machine[PlayerType_WithPistol].states[PlayerState_Roll].connections[PlayerState_Roll] = true;

            typedef struct EnemyStateNode
            {
                bool connections[EnemyState_COUNT];
            } EnemyStateNode;

            typedef struct EnemyStateMachine
            {
                EnemyStateNode states[EnemyState_COUNT];
            } EnemyStateMachine;

            EnemyStateMachine enemy_state_machine[EnemyType_COUNT] = {0};
            enemy_state_machine[EnemyType_Normal].states[EnemyState_Idle].connections[EnemyState_Idle] = true;
            enemy_state_machine[EnemyType_Normal].states[EnemyState_Idle].connections[EnemyState_Walk] = true;

            enemy_state_machine[EnemyType_Normal].states[EnemyState_Walk].connections[EnemyState_Idle] = true;
            enemy_state_machine[EnemyType_Normal].states[EnemyState_Walk].connections[EnemyState_Walk] = true;

            kvase_EnemyArray enemy_list = {0};
            enemy_list.capacity = 100u;
            enemy_list.data = calloc(enemy_list.capacity, sizeof(kvase_EnemyEntity));

            f32 enemy_spawn_timer = 0.0f;
            f32 enemy_spawn_rate = 2.0f;

            // NOTE: Currently just allow one action per frame. If more than one
            // input is processed this frame only the one with the highest priority
            // is processed.
            // TODO: Handle multiple inputs per frame.
            typedef enum ActionType
            {
                ActionType_None,
                ActionType_Attack,
                ActionType_Roll,
                ActionType_Move,
            } ActionType;

            typedef struct
            {
                ActionType type;
                f32 move_x;
                f32 move_y;
            } ActionInfo;

            bool debug_show_collision_rects = false;

            typedef struct
            {
                Vec2 scale;
                Vec2 pos;
                // TODO: This shouldn't really be here
                // find a better method
                VkDescriptorSet desc_set_handle;
            } SpriteRenderData;

            typedef struct
            {
                SpriteRenderData *data;
                u32 capacity;
                u32 count;
            } SpriteRenderArray;

            SpriteRenderArray sprite_render_list = {0};
            sprite_render_list.capacity = enemy_list.capacity + 1;
            sprite_render_list.count = 0;
            sprite_render_list.data = calloc(sprite_render_list.capacity, sizeof(SpriteRenderData));

            kvase_Rect2D attack_collision_offsets[PlayerDirection_COUNT] = {0};
            {
                Vec2 attack_offset_min = {.width = 64.0f, .height = -8.0f};
                Vec2 attack_offset_max = {.width = 46.0f, .height = 56.0f};
                kvase_Rect2D offset_down  = {.min = attack_offset_min, .max = attack_offset_max};

                attack_offset_min = (Vec2){.width = 46.0f, .height = 56.0f};
                attack_offset_max = (Vec2){.width = 64.0f, .height = 0.0f};
                kvase_Rect2D offset_up    = {.min = attack_offset_min, .max = attack_offset_max};

                attack_offset_min = (Vec2){.width = 56.0f, .height = 30.0f};
                attack_offset_max = (Vec2){.width = 0.0f, .height = 64.0f};
                kvase_Rect2D offset_left  = {.min = attack_offset_min, .max = attack_offset_max};

                attack_offset_min = (Vec2){.width = 0.0f, .height = 24.0f};
                attack_offset_max = (Vec2){.width = 60.0f, .height = 64.0f};
                kvase_Rect2D offset_right  = {.min = attack_offset_min, .max = attack_offset_max};

                attack_collision_offsets[PlayerDirection_Down]  = offset_down;
                attack_collision_offsets[PlayerDirection_Up]    = offset_up;
                attack_collision_offsets[PlayerDirection_Left]  = offset_left;
                attack_collision_offsets[PlayerDirection_Right] = offset_right;
            }

            Debug_EmitterPool *emitter_pool = calloc(1, sizeof(Debug_EmitterPool));

            Vec2 next_spawn_pos = {.x = GetRandomBetweenF32(-512.0f, 512.0f), .y = GetRandomBetweenF32(-512.0f, 512.0f)};

            while(app_running)
            {
                LARGE_INTEGER frame_start_time;
                QueryPerformanceCounter(&frame_start_time);
                // NOTE: start frame_id on 1
                frame_id++;

                // NOTE: reset the debug vertex buffer
                // TODO: Handle this better
                debug_vert_buffer.count = 0;
                particle_vert_buffer.count = 0;
                sprite_render_list.count = 0;

                MSG msg;
                while(PeekMessageA(&msg, NULL, 0, 0, PM_REMOVE))
                {
                    u8 local_id = 1;
                    switch(msg.message)
                    {
                        case WM_QUIT:
                        {
                            app_running = false;
                        } break;
                        case WM_KEYDOWN:
                        {
                            switch(msg.wParam)
                            {
                                case 'W':
                                {
                                    input_key_map[InputKey_W].down = true;
                                } break;
                                case 'S':
                                {
                                    input_key_map[InputKey_S].down = true;
                                } break;
                                case 'A':
                                {
                                    input_key_map[InputKey_A].down = true;
                                } break;
                                case 'D':
                                {
                                    input_key_map[InputKey_D].down = true;
                                } break;
                                case 'F':
                                {
                                    input_key_map[InputKey_F].down = true;
                                } break;
                                case 'T':
                                {
                                    input_key_map[InputKey_T].down = true;
                                } break;
                                case VK_SPACE:
                                {
                                    input_key_map[InputKey_Space].down = true;
                                } break;
                                case VK_TAB:
                                {
                                    input_key_map[InputKey_Tab].down = true;
                                }
                            }
                        } break;
                        case WM_KEYUP:
                        {
                            switch(msg.wParam)
                            {
                                case 'W':
                                {
                                    input_key_map[InputKey_W].down = false;
                                } break;
                                case 'S':
                                {
                                    input_key_map[InputKey_S].down = false;
                                } break;
                                case 'A':
                                {
                                    input_key_map[InputKey_A].down = false;
                                } break;
                                case 'D':
                                {
                                    input_key_map[InputKey_D].down = false;
                                } break;
                                case 'F':
                                {
                                    input_key_map[InputKey_F].down = false;
                                } break;
                                case 'T':
                                {
                                    input_key_map[InputKey_T].down = false;
                                } break;
                                case VK_SPACE:
                                {
                                    input_key_map[InputKey_Space].down = false;
                                } break;
                                case VK_TAB:
                                {
                                    input_key_map[InputKey_Tab].down = false;
                                }
                            }
                        } break;
                        default:
                        {
                            TranslateMessage(&msg);
                            DispatchMessage(&msg);
                        }
                    }
                }

                // TODO: handle this better
                if(!app_running)
                {
                    continue;
                }

                bool any_down = false;
                for(u32 i = 0; i < array_count(input_key_map); i++)
                {
                    if(input_key_map[i].down)
                    {
                        any_down = true;
                        if(input_key_map[i].down_time == -1.0f)
                        {
                            input_key_map[i].down_time = 0.0f;
                        }
                        else
                        {
                            input_key_map[i].down_time += dt;
                        }
                    }
                    else
                    {
                        input_key_map[i].down_time = -1.0f;
                    }
                }

                ActionInfo action_info = {0};

                if(any_down)
                {
                    // NOTE: Keys where the action is pressed is always prioritized higher
                    // than those that are usually held down.
                    if(input_key_map[InputKey_F].down_time > -1.0f)
                    {
                        action_info.type = ActionType_Attack;
                    }
                    else if(input_key_map[InputKey_Space].down_time > -1.0f)
                    {
                        action_info.type = ActionType_Roll;
                    }
                    else if(input_key_map[InputKey_W].down_time > -1.0f ||
                            input_key_map[InputKey_S].down_time > -1.0f ||
                            input_key_map[InputKey_A].down_time > -1.0f ||
                            input_key_map[InputKey_D].down_time > -1.0f)
                    {
                        action_info.type = ActionType_Move;
                        action_info.move_x = (input_key_map[InputKey_A].down_time > -1.0f ? -1.0f : 0.0f) + (input_key_map[InputKey_D].down_time > -1.0f ? 1.0f : 0.0f);
                        action_info.move_y = (input_key_map[InputKey_W].down_time > -1.0f ? -1.0f : 0.0f) + (input_key_map[InputKey_S].down_time > -1.0f ? 1.0f : 0.0f);
                    }

                    if(input_key_map[InputKey_Tab].down_time == 0.0f)
                    {
                        debug_show_collision_rects = !debug_show_collision_rects;
                    }
                }

                if(player->type != PlayerType_Dead)
                {
#if 1
                    if(enemy_spawn_timer > enemy_spawn_rate)
                    {
#if 1
                        enemy_spawn_timer = 0.0f;
                        kvase_EnemyEntity *e = SpawnEnemy(&enemy_list, next_spawn_pos);
                        // TODO: All spawn init should happen in the same place. Right
                        // now the animation is set here while other members are set
                        // inside the spawn function.
                        UpdateEnemyCurrentAnimation(e, enemy_animations);

                        next_spawn_pos.x = GetRandomBetweenF32(-512.0f, 512.0f);
                        next_spawn_pos.y = GetRandomBetweenF32(-512.0f, 512.0f);
#endif
                    }
                    else
                    {
                        enemy_spawn_timer += dt;
                    }
#endif
                }

                // NOTE: Update player
                PlayerType player_prev_type = player->type;
                PlayerState player_prev_state = player->state;
                PlayerDirection player_prev_face_dir = player->face_dir;
                {

                    if(player->type != PlayerType_Dead)
                    {

                        PlayerState next_state = PlayerState_Idle;
                        if(action_info.type == ActionType_None)
                        {
                            next_state = PlayerState_Idle;
                        }
                        else if(action_info.type == ActionType_Move)
                        {
                            if(action_info.move_x != 0.0f || action_info.move_y != 0.0f)
                            {
                                next_state = PlayerState_Walk;
                            }
                            else
                            {
                                next_state = PlayerState_Idle;
                            }
                        }
                        else if(action_info.type == ActionType_Attack)
                        {
                            next_state = PlayerState_Attack;
                        }
                        else if(action_info.type == ActionType_Roll)
                        {
                            next_state = PlayerState_Roll;
                        }
                        else
                        {
                            assert(false);
                        }

                        if(player->current_anim.loop_count >= player->current_anim.spec->required_loop_count)
                        {
                            if(player_state_machine[player->type].states[player->state].connections[next_state])
                            {
                                if(player->state != next_state)
                                {
                                    player->state = next_state;
                                }
                            }
                        }

                        if(player->state == PlayerState_Walk)
                        {
                            if(action_info.move_y < 0.0f)
                            {
                                player->face_dir = PlayerDirection_Up;
                            }
                            else if(action_info.move_y > 0.0f)
                            {
                                player->face_dir = PlayerDirection_Down;
                            }
                            else if(action_info.move_x < 0.0f)
                            {
                                player->face_dir = PlayerDirection_Left;
                            }
                            else if(action_info.move_x > 0.0f)
                            {
                                player->face_dir = PlayerDirection_Right;
                            }
                            else
                            {
                                // NOTE: Default is down sprite.
                                player->face_dir = PlayerDirection_Down;
                            }
                        }

                        if(player->state == PlayerState_Walk)
                        {
                            f32 player_speed = 500.0f;
                            Vec2 new_move_dir = vec2_Normalize((Vec2){.x = action_info.move_x, .y = action_info.move_y});
                            Vec2 vel = vec2_MulF32(new_move_dir, player_speed * dt);
                            player->pos = vec2_Add(player->pos, vel);
                            player->prev_move_dir = new_move_dir;
                        }
                        else if(player->state == PlayerState_Roll)
                        {
                            f32 player_speed = 700.0f;
                            Vec2 vel = vec2_MulF32(player->prev_move_dir, player_speed * dt);
                            player->pos = vec2_Add(player->pos, vel);
                        }
                        else if(player->state == PlayerState_Attack)
                        {
                            if(player->type == PlayerType_WithSwordAndShield)
                            {
                                if(player_prev_state != player->state)
                                {
                                    // TODO: Init the sword collision
                                }
                                else
                                {
                                    // TODO: Update the sword collision
                                }
                            }
                        }
                    }
                }

                // NOTE: Update enemies
                {

                    // TODO: Currently this "path finding" code is just
                    // a placehold and not good at all.
                    for(u32 ei = 0; ei < enemy_list.count; ei++)
                    {
                        kvase_EnemyEntity *enemy = &enemy_list.data[ei];
                        enemy->prev_state = enemy->state;
                        enemy->prev_face_dir = enemy->face_dir;
                        if(player->type != PlayerType_Dead)
                        {
                            if(enemy->target_update_timer > 0.1f)
                            {
                                enemy->target = player->pos;
                                enemy->target_update_timer = 0.0f;
                            }
                            else
                            {
                                enemy->target_update_timer += dt;
                            }

                            f32 dist = vec2_Distance(enemy->pos, enemy->target);
#if 1
                            if(dist > 20.0f)
                            {
                                enemy->state = EnemyState_Walk;
                            }
                            else
                            {
                                enemy->state = EnemyState_Idle;
                            }
#endif

                            if(enemy->state == EnemyState_Walk)
                            {
                                f32 speed = 300.0f;
                                Vec2 sides = vec2_Sub(enemy->target, enemy->pos);
                                f32 sign_x = sides.x >= 0.0f ? 1.0f : -1.0f;
                                f32 sign_y = sides.y >= 0.0f ? 1.0f : -1.0f;
                                sides = vec2_Abs(sides);

                                Vec2 dir = {0};
                                // TODO: This target should decrease as the distance between
                                // enemy and player gets smaller.
                                const f32 treshold_size = 10.0f;
                                if(sides.x < treshold_size)
                                {
                                    dir.y = sign_y;
                                }
                                else if(sides.y < treshold_size)
                                {
                                    dir.x = sign_x;
                                }
                                else
                                {
                                    dir.x = sign_x;
                                    dir.y = sign_y;
                                    dir = vec2_Normalize(dir);
                                }

                                Vec2 vel = vec2_MulF32(dir, speed * dt);
                                enemy->pos = vec2_Add(enemy->pos, vel);

                                if(dir.y > 0.0f)
                                {
                                    enemy->face_dir = EnemyDirection_Down;
                                }
                                else if(dir.y < 0.0f)
                                {
                                    enemy->face_dir = EnemyDirection_Up;
                                }
                                else if(dir.x == -1.0f)
                                {
                                    enemy->face_dir = EnemyDirection_Left;
                                }
                                else if(dir.x == 1.0f)
                                {
                                    enemy->face_dir = EnemyDirection_Right;
                                }
                            }
                        }
                    }
                }

                // NOTE: Update particles generated by enemy death
                {
                    for(u32 a = 0; a < array_count(emitter_pool->items); a++)
                    {
                        Debug_ParticleEmitter *em = &emitter_pool->items[a];
                        if(em->active)
                        {
                            em->time_alive += dt;
                            if(em->time_alive < em->time_to_live)
                            {
                                for(u32 b = 0; b < array_count(em->particles); b++)
                                {
                                    Debug_Particle *p = &em->particles[b];
                                    p->vel.y += (10.0f * dt);
                                    Vec2 old_pos = p->pos;
                                    Vec2 new_pos = vec2_Add(p->pos, p->vel);
                                    if(new_pos.y >= p->baseline.y)
                                    {
                                        Vec2 n = {.x = 0.0f, .y = 1.0f};
                                        Vec2 r = vec2_MulF32(n, vec2_Dot(p->vel, n));
                                        r = vec2_MulF32(r, -2);
                                        r = vec2_Add(r, p->vel);
                                        p->vel = r;

                                        f32 t = p->bounce_count / 10.0f;
                                        if(t < 1.0f)
                                        {
                                            f32 s = (1.0f - t);
                                            p->vel = vec2_MulF32(p->vel, s);
                                        }
                                        else
                                        {
                                            p->vel = (Vec2){.x = 0.0f, .y = 0.0f};
                                        }

                                        f32 y_hit = p->baseline.y;
                                        f32 x_hit = old_pos.x + ((y_hit - old_pos.y) * (new_pos.x - old_pos.x) / (new_pos.y - old_pos.y));
                                        Vec2 hit_pos = {.x = x_hit, .y = y_hit};
                                        p->pos = vec2_Add(hit_pos, p->vel);

                                        p->bounce_count += 1.0f;
                                    }
                                    else
                                    {
                                        p->pos = new_pos;
                                    }
                                }
                            }
                            else
                            {
                                em->active = false;
                            }
                        }
                    }
                }
#if 0
                f32 start = -1024.0f;
                f32 end = 1024.0f;
                for(f32 y = start; y < end; y +=64.0f)
                {
                    kvase_AddLine(&debug_vert_buffer, (Vec2){.x = start, .y = y}, (Vec2){.x = end, .y = y}, (Vec4){.r = 1.0f, .b = 1.0f, .a = 1.0f});
                }
                for(f32 x = start; x < end; x += 64.0f)
                {
                    kvase_AddLine(&debug_vert_buffer, (Vec2){.x = x, .y = start}, (Vec2){.x = x, .y = end}, (Vec4){.r = 1.0f, .b = 1.0f, .a = 1.0f});
                }
#endif

                if(player->type != PlayerType_Dead)
                {
                    if(player->state == PlayerState_Attack && player->type == PlayerType_WithSwordAndShield)
                    {
                        kvase_Rect2D offset = attack_collision_offsets[player->face_dir];
                        kvase_Rect2D attack_col  = {.min = vec2_Sub(player->pos, offset.min), .max = vec2_Add(player->pos, offset.max)};

                        if(enemy_list.count > 0)
                        {
                            for(i32 ei = (i32)(enemy_list.count-1); ei >= 0; ei--)
                            {
                                Vec4 debug_col = {.r = 1.0f, .a = 1.0f};
                                kvase_EnemyEntity *enemy = &enemy_list.data[ei];
                                Vec2 half_scale = (Vec2){.x = 16.0f * (enemy->scale.x / 64.0f) * 0.22f, .y = 16.0f * (enemy->scale.y / 64.0f) * 0.25f};
                                kvase_Rect2D enemy_collision_rect = {.min = vec2_Sub(enemy->pos, half_scale), .max = vec2_Add(enemy->pos, half_scale)};
                                enemy_collision_rect.min.y += 16.0f;
                                enemy_collision_rect.max.y += 4.0f;

                                if(attack_col.min.x < enemy_collision_rect.max.x &&
                                   attack_col.max.x > enemy_collision_rect.min.x &&
                                   attack_col.min.y < enemy_collision_rect.max.y &&
                                   attack_col.max.y > enemy_collision_rect.min.y)
                                {
                                    // TODO: Make a better system so that it's easier to
                                    // handle things like making an enemy no longer active.
                                    Debug_ParticleEmitter *emitter = Debug_EmitterPool_FindFree(emitter_pool);
                                    emitter->active = true;
                                    emitter->time_to_live = 6.0f;
                                    _Static_assert(array_count(emitter->particles) == 15, "Incorrect array count found [particle emitter]");
                                    for(u32 a = 0; a < array_count(emitter->particles); a++)
                                    {
                                        emitter->particles[a].vel = (Vec2){.x = GetRandomBetweenF32(-50.0f, 50.0f) * dt, .y = GetRandomBetweenF32(-200.0f, -50.0f) * dt};
                                        emitter->particles[a].col = (Vec4){.r = 1.0f, .g = 0.0f, .a = 1.0f};
                                        emitter->particles[a].baseline = (Vec2){.x = enemy->pos.x, .y = enemy->pos.y + GetRandomBetweenF32(-28.0f, 28.0f)};
                                        emitter->particles[a].pos = emitter->particles[a].baseline;
                                    }


                                    // NOTE: just debug, make sure no one uses it after
                                    // it's been deleted
                                    enemy = NULL;
                                    // NOTE: delete enemy from list;
                                    enemy_list.data[ei] = enemy_list.data[--enemy_list.count];
                                }
                            }
                        }
                    }

                    Vec2 half_scale = {.x = 16.0f * (player->scale.width / 64.0f) * 0.22f, .y = 16.0f * (player->scale.height / 64.0f) * 0.25f};
                    kvase_Rect2D player_collision_rect = {.min = vec2_Sub(player->pos, half_scale), .max = vec2_Add(player->pos, half_scale)};
                    player_collision_rect.min.y += 16.0f;
                    player_collision_rect.max.y += 4.0f;

                    bool any_hit = false;
                    for(u32 ei = 0; ei < enemy_list.count; ei++)
                    {
                        Vec4 debug_col = {.r = 1.0f, .a = 1.0f};
                        kvase_EnemyEntity *enemy = &enemy_list.data[ei];
                        half_scale = (Vec2){.x = 16.0f * (enemy->scale.x / 64.0f) * 0.22f, .y = 16.0f * (enemy->scale.y / 64.0f) * 0.25f};
                        kvase_Rect2D enemy_collision_rect = {.min = vec2_Sub(enemy->pos, half_scale), .max = vec2_Add(enemy->pos, half_scale)};
                        enemy_collision_rect.min.y += 16.0f;
                        enemy_collision_rect.max.y += 4.0f;

                        if(player_collision_rect.min.x < enemy_collision_rect.max.x &&
                           player_collision_rect.max.x > enemy_collision_rect.min.x &&
                           player_collision_rect.min.y < enemy_collision_rect.max.y &&
                           player_collision_rect.max.y > enemy_collision_rect.min.y)
                        {
                            debug_col.r = 0.0f;
                            debug_col.b = 1.0f;
                            player->type = PlayerType_Dead;
                            any_hit = true;
                            break;
                        }

                        if(debug_show_collision_rects)
                        {
                            kvase_AddRect(&debug_vert_buffer, enemy_collision_rect.min, enemy_collision_rect.max, debug_col);
                        }
                    }

                    if(player->type == PlayerType_Dead)
                    {
                        for(u32 ei = 0; ei < enemy_list.count; ei++)
                        {
                            enemy_list.data[ei].state = EnemyState_Idle;
                        }
                    }

                    if(debug_show_collision_rects)
                    {
                        Vec4 debug_col = {.r = 1.0f, .a = 1.0f};
                        if(any_hit)
                        {
                            debug_col.r = 0.0f;
                            debug_col.b = 1.0f;
                        }

                        kvase_AddRect(&debug_vert_buffer, player_collision_rect.min, player_collision_rect.max, debug_col);

                        if(player->state == PlayerState_Attack && player->type == PlayerType_WithSwordAndShield)
                        {
                            debug_col.r = 0.0f;
                            debug_col.b = 1.0f;
                            kvase_Rect2D offset = attack_collision_offsets[player->face_dir];
                            kvase_Rect2D attack_col  = {.min = vec2_Sub(player->pos, offset.min), .max = vec2_Add(player->pos, offset.max)};
                            kvase_AddRect(&debug_vert_buffer, attack_col.min, attack_col.max, debug_col);
                        }

                        assert(debug_vert_buffer.count > 0);
                    }

                    {
#if 1
                        // TODO: The particles "live" in the same world as other
                        // entities in the world, so needs to be sorted accordingly.
                        // Currently they are just drawn into the debug layer which
                        // is always on top.
                        for(u32 a = 0; a < array_count(emitter_pool->items); a++)
                        {
                            Debug_ParticleEmitter *em = &emitter_pool->items[a];
                            if(em->active)
                            {
                                for(u32 b = 0; b < array_count(em->particles); b++)
                                {
                                    Debug_Particle *p = &em->particles[b];
                                    kvase_Rect2D r = CreateRect2D(p->pos, (Vec2){.width = 4.0f, .height = 4.0f});
                                    kvase_AddRectFilled(&particle_vert_buffer, r.min, r.max, p->col);
                                }
                            }
                        }
#endif
                    }

                    {
                        f32 alpha = (enemy_spawn_timer / enemy_spawn_rate);
                        kvase_Rect2D r = CreateRect2D(next_spawn_pos, (Vec2){.width = 32.0f, .height = 32.0f});
                        Vec4 spawn_col = {.r = 1.0f, .g = 1.0f, .a = alpha};
                        kvase_AddRectFilled(&debug_vert_buffer, r.min, r.max, spawn_col);
                    }

                    if(debug_vert_buffer.count > 0)
                    {
                        memcpy(debug_pipe_vk_vert_buffer.mem_mapped, debug_vert_buffer.data, debug_vert_buffer.count * debug_vert_buffer.unit_size);
                    }
                    if(particle_vert_buffer.count > 0)
                    {
                        memcpy(particle_pipe_vk_vert_buffer.mem_mapped, particle_vert_buffer.data, particle_vert_buffer.count * particle_vert_buffer.unit_size);
                    }
                }


                // NOTE: Update animations
                {
                    // NOTE: Player Animations
                    {
                        bool direction_changed = player_prev_face_dir != player->face_dir;
                        bool state_changed = player_prev_state != player->state;
                        bool type_changed = player_prev_type != player->type;
                        if(type_changed || state_changed || direction_changed)
                        {
                            // NOTE: Getting the animation is not necessary
                            // when just the face direction is changed.
                            UpdatePlayerCurrentAnimation(player);
                        }

                        PlayPlayerAnimation(player, dt);
                    }

                    // NOTE: Enemy animations
                    {

                        for(u32 ei = 0; ei < enemy_list.count; ei++)
                        {
                            kvase_EnemyEntity *enemy = &enemy_list.data[ei];
                            bool state_changed = enemy->prev_state != enemy->state;
                            bool face_dir_changed = enemy->prev_face_dir != enemy->face_dir;
                            if(state_changed || face_dir_changed)
                            {
                                UpdateEnemyCurrentAnimation(enemy, enemy_animations);
                            }

                            PlayEnemyAnimation(enemy, dt);
                        }
                    }
                }

                {


                    kvase_UpdateDefaultTextureDescriptor(&vulkan_ctx, player->current_anim.spec->frames[player->current_anim.frame_index].image_view_handle, default_sprite_sampler_handle, sprite_pipe_texture_descs.set_handles[0]);

                    assert(sprite_render_list.count == 0);
                    sprite_render_list.data[sprite_render_list.count++] = (SpriteRenderData){.pos = player->pos, .scale = player->scale, .desc_set_handle = sprite_pipe_texture_descs.set_handles[0]};

#if 1
                    for(u32 ei = 0; ei < enemy_list.count; ei++)
                    {
                        u32 desc_index = ei+1;
                        assert(desc_index < 1000);
                        assert(sprite_render_list.count < sprite_render_list.capacity);

                        kvase_EnemyEntity *e = &enemy_list.data[ei];
                        kvase_UpdateDefaultTextureDescriptor(&vulkan_ctx, e->current_anim.spec->frames[e->current_anim.frame_index].image_view_handle, default_sprite_sampler_handle, sprite_pipe_texture_descs.set_handles[desc_index]);
                        sprite_render_list.data[sprite_render_list.count++] = (SpriteRenderData){.pos = e->pos, .scale = e->scale, .desc_set_handle = sprite_pipe_texture_descs.set_handles[desc_index]};
                    }
#endif

                    for(u32 a = 0; a < sprite_render_list.count-1; a++)
                    {
                        Vec2 val_a = sprite_render_list.data[a].pos;
                        u32 min_index = a;
                        for(u32 b = a+1; b < sprite_render_list.count; b++)
                        {
                            Vec2 val_b = sprite_render_list.data[b].pos;
                            if(val_b.y < val_a.y)
                            {
                                min_index = b;
                            }
                        }

                        if(min_index != a)
                        {
                            SpriteRenderData tmp = sprite_render_list.data[a];
                            sprite_render_list.data[a] = sprite_render_list.data[min_index];
                            sprite_render_list.data[min_index] = tmp;
                        }
                    }
                }


                Vec2U win_size = win32_GetWindowSizeU32(app_ctx.window_handle);

                // TODO: handle this better
                if(win_size.width == 0 || win_size.height == 0)
                {
                    continue;
                }
                Vec2U ext_size = {.width = vulkan_ctx.swapchain_extent.width, .height = vulkan_ctx.swapchain_extent.height};
                if(!v2u_Equal(win_size, ext_size))
                {
                    // NOTE: Destroy and recreate swapchain its resources
                    assert(win_size.width > 0 && win_size.height > 0);
                    DestroyPipeline(vulkan_ctx.device_handle, &sprite_pipeline);
                    DestroySwapchainResources(&vulkan_ctx);


                    CreateAndInitSwapchain(&app_ctx, &vulkan_ctx, win_size.width, win_size.height);
                    CreateAndInitDepthImageView(&vulkan_ctx, default_depth_format);
                    sprite_pipeline = CreateDefaultPipeline(&vulkan_ctx,
                                                            sprite_pipe_vert_shader_module, sprite_pipe_frag_shader_module,
                                                            sprite_pipe_desc_layouts, array_count(sprite_pipe_desc_layouts),
                                                            sprite_pipe_push_constant_configs, array_count(sprite_pipe_push_constant_configs));
                    CreateDefaultFramebuffers(&vulkan_ctx);
                }


                VkSemaphore present_done_sem = GetDefaultSemaphore(&vulkan_ctx, PresentDone);
                VkSemaphore render_done_sem = GetDefaultSemaphore(&vulkan_ctx, RenderDone);

                u32 next_image_index;
                KVASE_VK_CHECK(vkAcquireNextImageKHR(vulkan_ctx.device_handle,
                                                   vulkan_ctx.swapchain_handle,
                                                   UINT64_MAX,
                                                   present_done_sem,
                                                   VK_NULL_HANDLE,
                                                   &next_image_index));


                {
                    VkCommandBuffer render_cmd = GetDefaultCmd(&vulkan_ctx, Render);

                    VkCommandBufferBeginInfo present_cmd_begin_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                    };

                    KVASE_VK_CHECK(vkBeginCommandBuffer(render_cmd, &present_cmd_begin_info));

                    VkImageMemoryBarrier layout_trans_barrier =
                    {
                        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                        .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
                        .dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                        .newLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .image = vulkan_ctx.swapchain_images[next_image_index],
                        .subresourceRange =
                        {
                            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                            .baseMipLevel = 0,
                            .levelCount = 1,
                            .baseArrayLayer = 0,
                            .layerCount = 1,
                        },
                    };

                    vkCmdPipelineBarrier(render_cmd,
                                         VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                         VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                                         0,
                                         0, NULL,
                                         0, NULL,
                                         1, &layout_trans_barrier);


                    VkClearValue clear_values[] =
                    {
                        {.color = {.float32 = {0.5f, 0.5f, 0.8f, 1.0f}}},
                        {.depthStencil = {.depth = 1.0f, .stencil = 0}},
                    };
                    VkRenderPassBeginInfo render_pass_begin =
                    {
                        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
                        .renderPass = vulkan_ctx.default_render_pass_handle,
                        .framebuffer = vulkan_ctx.framebuffers[next_image_index],
                        .renderArea = {.offset = {0,0}, .extent = vulkan_ctx.swapchain_extent},
                        .clearValueCount = array_count(clear_values),
                        .pClearValues = clear_values,
                    };

                    vkCmdBeginRenderPass(render_cmd, &render_pass_begin, VK_SUBPASS_CONTENTS_INLINE);
                    VkViewport viewport =
                    {
                        .x = 0,
                        .y = 0,
                        .width = (f32)vulkan_ctx.swapchain_extent.width,
                        .height = (f32)vulkan_ctx.swapchain_extent.height,
                        .minDepth = 0.0f,
                        .maxDepth = 0.0f,
                    };

                    vkCmdSetViewport(render_cmd, 0, 1, &viewport);

                    VkRect2D scissor = {.offset = {0, 0}, .extent = vulkan_ctx.swapchain_extent};

                    vkCmdSetScissor(render_cmd, 0, 1, &scissor);

                    if(particle_vert_buffer.count > 0)
                    {
                        VkDeviceSize offset = 0;
                        vkCmdBindPipeline(render_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, debug_pipeline.pipeline_handle);
                        vkCmdBindVertexBuffers(render_cmd, 0, 1, &particle_pipe_vk_vert_buffer.buffer_handle, &offset);
                        vkCmdBindDescriptorSets(render_cmd,
                                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                                debug_pipeline.layout_handle,
                                                0,
                                                1,
                                                &debug_pipe_desc.set_handle,
                                                0,
                                                NULL);


                        vkCmdDraw(render_cmd, particle_vert_buffer.count, 1, 0, 0);
                    }

                    vkCmdBindPipeline(render_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, sprite_pipeline.pipeline_handle);

                    VkDeviceSize offset = 0;
                    vkCmdBindVertexBuffers(render_cmd, 0, 1, &vertex_buffer.buffer_handle, &offset);
                    vkCmdBindDescriptorSets(render_cmd,
                                            VK_PIPELINE_BIND_POINT_GRAPHICS,
                                            sprite_pipeline.layout_handle,
                                            0,
                                            1,
                                            &sprite_pipe_ubo_desc.set_handle,
                                            0,
                                            NULL);


                    for(u32 si = 0; si < sprite_render_list.count; si++)
                    {
                        vkCmdBindDescriptorSets(render_cmd,
                                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                                sprite_pipeline.layout_handle,
                                                1,
                                                1,
                                                &sprite_render_list.data[si].desc_set_handle,
                                                0,
                                                NULL);

                        Vec2 scale_and_trans[] =
                        {
                            sprite_render_list.data[si].scale,
                            sprite_render_list.data[si].pos,
                        };

                        vkCmdPushConstants(render_cmd,
                                           sprite_pipeline.layout_handle,
                                           VK_SHADER_STAGE_VERTEX_BIT,
                                           0,
                                           sizeof(Vec2) * 2,
                                           scale_and_trans);

                        vkCmdDraw(render_cmd, array_count(vertex_data), 1, 0, 0);
                    }

                    if(debug_vert_buffer.count > 0)
                    {
                        vkCmdBindPipeline(render_cmd, VK_PIPELINE_BIND_POINT_GRAPHICS, debug_pipeline.pipeline_handle);
                        vkCmdBindVertexBuffers(render_cmd, 0, 1, &debug_pipe_vk_vert_buffer.buffer_handle, &offset);
                        vkCmdBindDescriptorSets(render_cmd,
                                                VK_PIPELINE_BIND_POINT_GRAPHICS,
                                                debug_pipeline.layout_handle,
                                                0,
                                                1,
                                                &debug_pipe_desc.set_handle,
                                                0,
                                                NULL);


                        vkCmdDraw(render_cmd, debug_vert_buffer.count, 1, 0, 0);
                    }

                    vkCmdEndRenderPass(render_cmd);

                    VkImageMemoryBarrier present_trans_barrier =
                    {
                        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                        .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                        .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
                        .oldLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                        .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                        .image = vulkan_ctx.swapchain_images[next_image_index],
                        .subresourceRange =
                        {
                            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                            .baseMipLevel = 0,
                            .levelCount = 1,
                            .baseArrayLayer = 0,
                            .layerCount = 1,
                        },
                    };

                    vkCmdPipelineBarrier(render_cmd,
                                         VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                         VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                         0,
                                         0, NULL,
                                         0, NULL,
                                         1, &present_trans_barrier);

                    KVASE_VK_CHECK(vkEndCommandBuffer(render_cmd));

                    VkFence render_fence;
                    VkFenceCreateInfo render_fence_ci = {.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO};
                    KVASE_VK_CHECK(vkCreateFence(vulkan_ctx.device_handle, &render_fence_ci, NULL, &render_fence));

                    VkPipelineStageFlags wait_stage_mask = {VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT};
                    VkSubmitInfo render_submit_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                        .waitSemaphoreCount = 1,
                        .pWaitSemaphores = &present_done_sem,
                        .pWaitDstStageMask = &wait_stage_mask,
                        .signalSemaphoreCount = 1,
                        .pSignalSemaphores = &render_done_sem,
                        .commandBufferCount = 1,
                        .pCommandBuffers = &render_cmd,
                    };
                    KVASE_VK_CHECK(vkQueueSubmit(vulkan_ctx.device_queue_handle, 1, &render_submit_info, render_fence));

                    KVASE_VK_CHECK(vkWaitForFences(vulkan_ctx.device_handle, 1, &render_fence, VK_TRUE, UINT64_MAX));
                    KVASE_VK_CHECK(vkResetFences(vulkan_ctx.device_handle, 1, &render_fence));

                    VkPresentInfoKHR present_info =
                    {
                        .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
                        .waitSemaphoreCount = 1,
                        .pWaitSemaphores = &render_done_sem,
                        .swapchainCount = 1,
                        .pSwapchains = &vulkan_ctx.swapchain_handle,
                        .pImageIndices = &next_image_index,
                    };

                    KVASE_VK_CHECK(vkQueuePresentKHR(vulkan_ctx.device_queue_handle, &present_info));

                    KVASE_VK_CHECK(vkResetCommandBuffer(render_cmd, 0));

                    vkDestroyFence(vulkan_ctx.device_handle, render_fence, NULL);
                }

                dt = win32_GetElapsedSeconds(frame_start_time, perf_count_freq);
                runtime += dt;

                char output_buffer[256];
                sprintf_s(output_buffer, 256, "frame time: %f\n", (double)dt);
                OutputDebugString(output_buffer);
            }
        }
    }

    return 0;
}
