#ifndef KVASE_ASSET_PATHS_H
#define KVASE_ASSET_PATHS_H


static char *paths_char_without_weapon_idle_down[] =
{
    "../assets/character/character_without_weapon/idle/idle down1.png",
    "../assets/character/character_without_weapon/idle/idle down2.png",
    "../assets/character/character_without_weapon/idle/idle down3.png",
    "../assets/character/character_without_weapon/idle/idle down4.png",
};
#define ANIM_ID_0 paths_char_without_weapon_idle_down

static char *paths_char_without_weapon_idle_right[] =
{
    "../assets/character/character_without_weapon/idle/idle right1.png",
    "../assets/character/character_without_weapon/idle/idle right2.png",
    "../assets/character/character_without_weapon/idle/idle right3.png",
    "../assets/character/character_without_weapon/idle/idle right4.png",
};
#define ANIM_ID_1 paths_char_without_weapon_idle_right

static char *paths_char_without_weapon_idle_left[] =
{
    "../assets/character/character_without_weapon/idle/idle left1.png",
    "../assets/character/character_without_weapon/idle/idle left2.png",
    "../assets/character/character_without_weapon/idle/idle left3.png",
    "../assets/character/character_without_weapon/idle/idle left4.png",
};
#define ANIM_ID_2 paths_char_without_weapon_idle_left

static char *paths_char_without_weapon_idle_up[] =
{
    "../assets/character/character_without_weapon/idle/idle up1.png",
    "../assets/character/character_without_weapon/idle/idle up2.png",
    "../assets/character/character_without_weapon/idle/idle up3.png",
    "../assets/character/character_without_weapon/idle/idle up4.png",
};
#define ANIM_ID_3 paths_char_without_weapon_idle_up

static char *paths_char_without_weapon_walk_down[] =
{
    "../assets/character/character_without_weapon/walk/walk down1.png",
    "../assets/character/character_without_weapon/walk/walk down2.png",
    "../assets/character/character_without_weapon/walk/walk down3.png",
    "../assets/character/character_without_weapon/walk/walk down4.png",
};
#define ANIM_ID_4 paths_char_without_weapon_walk_down

static char *paths_char_without_weapon_walk_right[] =
{
    "../assets/character/character_without_weapon/walk/walk right1.png",
    "../assets/character/character_without_weapon/walk/walk right2.png",
    "../assets/character/character_without_weapon/walk/walk right3.png",
    "../assets/character/character_without_weapon/walk/walk right4.png",
};
#define ANIM_ID_5 paths_char_without_weapon_walk_right

static char *paths_char_without_weapon_walk_left[] =
{
    "../assets/character/character_without_weapon/walk/walk left1.png",
    "../assets/character/character_without_weapon/walk/walk left2.png",
    "../assets/character/character_without_weapon/walk/walk left3.png",
    "../assets/character/character_without_weapon/walk/walk left4.png",
};
#define ANIM_ID_6 paths_char_without_weapon_walk_left

static char *paths_char_without_weapon_walk_up[] =
{
    "../assets/character/character_without_weapon/walk/walk up1.png",
    "../assets/character/character_without_weapon/walk/walk up2.png",
    "../assets/character/character_without_weapon/walk/walk up3.png",
    "../assets/character/character_without_weapon/walk/walk up4.png",
};
#define ANIM_ID_7 paths_char_without_weapon_walk_up

static char *paths_char_without_weapon_attack_down[] =
{
    "../assets/character/character_without_weapon/attack/attack down1.png",
    "../assets/character/character_without_weapon/attack/attack down2.png",
    "../assets/character/character_without_weapon/attack/attack down3.png",
    "../assets/character/character_without_weapon/attack/attack down4.png",
};
#define ANIM_ID_8 paths_char_without_weapon_attack_down

static char *paths_char_without_weapon_attack_right[] =
{
    "../assets/character/character_without_weapon/attack/attack right1.png",
    "../assets/character/character_without_weapon/attack/attack right2.png",
    "../assets/character/character_without_weapon/attack/attack right3.png",
    "../assets/character/character_without_weapon/attack/attack right4.png",
};
#define ANIM_ID_9 paths_char_without_weapon_attack_right

static char *paths_char_without_weapon_attack_left[] =
{
    "../assets/character/character_without_weapon/attack/attack left1.png",
    "../assets/character/character_without_weapon/attack/attack left2.png",
    "../assets/character/character_without_weapon/attack/attack left3.png",
    "../assets/character/character_without_weapon/attack/attack left4.png",
};
#define ANIM_ID_10 paths_char_without_weapon_attack_left

static char *paths_char_without_weapon_attack_up[] =
{
    "../assets/character/character_without_weapon/attack/attack up1.png",
    "../assets/character/character_without_weapon/attack/attack up2.png",
    "../assets/character/character_without_weapon/attack/attack up3.png",
    "../assets/character/character_without_weapon/attack/attack up4.png",
};
#define ANIM_ID_11 paths_char_without_weapon_attack_up

static char *paths_char_without_weapon_roll_down[] =
{
    "../assets/character/character_without_weapon/roll/roll down1.png",
    "../assets/character/character_without_weapon/roll/roll down2.png",
    "../assets/character/character_without_weapon/roll/roll down3.png",
    "../assets/character/character_without_weapon/roll/roll down4.png",
};
#define ANIM_ID_12 paths_char_without_weapon_roll_down

static char *paths_char_without_weapon_roll_right[] =
{
    "../assets/character/character_without_weapon/roll/roll right1.png",
    "../assets/character/character_without_weapon/roll/roll right2.png",
    "../assets/character/character_without_weapon/roll/roll right3.png",
    "../assets/character/character_without_weapon/roll/roll right4.png",
};
#define ANIM_ID_13 paths_char_without_weapon_roll_right

static char *paths_char_without_weapon_roll_left[] =
{
    "../assets/character/character_without_weapon/roll/roll left1.png",
    "../assets/character/character_without_weapon/roll/roll left2.png",
    "../assets/character/character_without_weapon/roll/roll left3.png",
    "../assets/character/character_without_weapon/roll/roll left4.png",
};
#define ANIM_ID_14 paths_char_without_weapon_roll_left

static char *paths_char_without_weapon_roll_up[] =
{
    "../assets/character/character_without_weapon/roll/roll up1.png",
    "../assets/character/character_without_weapon/roll/roll up2.png",
    "../assets/character/character_without_weapon/roll/roll up3.png",
    "../assets/character/character_without_weapon/roll/roll up4.png",
};
#define ANIM_ID_15 paths_char_without_weapon_roll_up

static char *paths_char_with_sword_and_shield_idle_down[] =
{
    "../assets/character/character_with_sword_and_shield/idle/idle down1.png",
    "../assets/character/character_with_sword_and_shield/idle/idle down2.png",
    "../assets/character/character_with_sword_and_shield/idle/idle down3.png",
    "../assets/character/character_with_sword_and_shield/idle/idle down4.png",
};
#define ANIM_ID_16 paths_char_with_sword_and_shield_idle_down

static char *paths_char_with_sword_and_shield_idle_left[] =
{
    "../assets/character/character_with_sword_and_shield/idle/idle left1.png",
    "../assets/character/character_with_sword_and_shield/idle/idle left2.png",
    "../assets/character/character_with_sword_and_shield/idle/idle left3.png",
    "../assets/character/character_with_sword_and_shield/idle/idle left4.png",
};
#define ANIM_ID_17 paths_char_with_sword_and_shield_idle_left

static char *paths_char_with_sword_and_shield_idle_right[] =
{
    "../assets/character/character_with_sword_and_shield/idle/idle right1.png",
    "../assets/character/character_with_sword_and_shield/idle/idle right2.png",
    "../assets/character/character_with_sword_and_shield/idle/idle right3.png",
    "../assets/character/character_with_sword_and_shield/idle/idle right4.png",
};
#define ANIM_ID_18 paths_char_with_sword_and_shield_idle_right

static char *paths_char_with_sword_and_shield_idle_up[] =
{
    "../assets/character/character_with_sword_and_shield/idle/idle up1.png",
    "../assets/character/character_with_sword_and_shield/idle/idle up2.png",
    "../assets/character/character_with_sword_and_shield/idle/idle up3.png",
    "../assets/character/character_with_sword_and_shield/idle/idle up4.png",
};
#define ANIM_ID_19 paths_char_with_sword_and_shield_idle_up

static char *paths_char_with_sword_and_shield_walk_down[] =
{
    "../assets/character/character_with_sword_and_shield/walk/walk down1.png",
    "../assets/character/character_with_sword_and_shield/walk/walk down2.png",
    "../assets/character/character_with_sword_and_shield/walk/walk down3.png",
    "../assets/character/character_with_sword_and_shield/walk/walk down4.png",
};
#define ANIM_ID_20 paths_char_with_sword_and_shield_walk_down

static char *paths_char_with_sword_and_shield_walk_left[] =
{
    "../assets/character/character_with_sword_and_shield/walk/walk left1.png",
    "../assets/character/character_with_sword_and_shield/walk/walk left2.png",
    "../assets/character/character_with_sword_and_shield/walk/walk left3.png",
    "../assets/character/character_with_sword_and_shield/walk/walk left4.png",
};
#define ANIM_ID_21 paths_char_with_sword_and_shield_walk_left

static char *paths_char_with_sword_and_shield_walk_right[] =
{
    "../assets/character/character_with_sword_and_shield/walk/walk right1.png",
    "../assets/character/character_with_sword_and_shield/walk/walk right2.png",
    "../assets/character/character_with_sword_and_shield/walk/walk right3.png",
    "../assets/character/character_with_sword_and_shield/walk/walk right4.png",
};
#define ANIM_ID_22 paths_char_with_sword_and_shield_walk_right

static char *paths_char_with_sword_and_shield_walk_up[] =
{
    "../assets/character/character_with_sword_and_shield/walk/walk up1.png",
    "../assets/character/character_with_sword_and_shield/walk/walk up2.png",
    "../assets/character/character_with_sword_and_shield/walk/walk up3.png",
    "../assets/character/character_with_sword_and_shield/walk/walk up4.png",
};
#define ANIM_ID_23 paths_char_with_sword_and_shield_walk_up

static char *paths_char_with_sword_and_shield_attack_down[] =
{
    "../assets/character/character_with_sword_and_shield/attack/attack down1.png",
    "../assets/character/character_with_sword_and_shield/attack/attack down2.png",
    "../assets/character/character_with_sword_and_shield/attack/attack down3.png",
    "../assets/character/character_with_sword_and_shield/attack/attack down4.png",
};
#define ANIM_ID_24 paths_char_with_sword_and_shield_attack_down

static char *paths_char_with_sword_and_shield_attack_left[] =
{
    "../assets/character/character_with_sword_and_shield/attack/attack left1.png",
    "../assets/character/character_with_sword_and_shield/attack/attack left2.png",
    "../assets/character/character_with_sword_and_shield/attack/attack left3.png",
    "../assets/character/character_with_sword_and_shield/attack/attack left4.png",
};
#define ANIM_ID_25 paths_char_with_sword_and_shield_attack_left

static char *paths_char_with_sword_and_shield_attack_right[] =
{
    "../assets/character/character_with_sword_and_shield/attack/attack right1.png",
    "../assets/character/character_with_sword_and_shield/attack/attack right2.png",
    "../assets/character/character_with_sword_and_shield/attack/attack right3.png",
    "../assets/character/character_with_sword_and_shield/attack/attack right4.png",
};
#define ANIM_ID_26 paths_char_with_sword_and_shield_attack_right

static char *paths_char_with_sword_and_shield_attack_up[] =
{
    "../assets/character/character_with_sword_and_shield/attack/attack up1.png",
    "../assets/character/character_with_sword_and_shield/attack/attack up2.png",
    "../assets/character/character_with_sword_and_shield/attack/attack up3.png",
    "../assets/character/character_with_sword_and_shield/attack/attack up4.png",
};
#define ANIM_ID_27 paths_char_with_sword_and_shield_attack_up

static char *paths_char_with_sword_and_shield_roll_down[] =
{
    "../assets/character/character_with_sword_and_shield/roll/roll down1.png",
    "../assets/character/character_with_sword_and_shield/roll/roll down2.png",
    "../assets/character/character_with_sword_and_shield/roll/roll down3.png",
    "../assets/character/character_with_sword_and_shield/roll/roll down4.png",
};
#define ANIM_ID_28 paths_char_with_sword_and_shield_roll_down

static char *paths_char_with_sword_and_shield_roll_left[] =
{
    "../assets/character/character_with_sword_and_shield/roll/roll left1.png",
    "../assets/character/character_with_sword_and_shield/roll/roll left2.png",
    "../assets/character/character_with_sword_and_shield/roll/roll left3.png",
    "../assets/character/character_with_sword_and_shield/roll/roll left4.png",
};
#define ANIM_ID_29 paths_char_with_sword_and_shield_roll_left

static char *paths_char_with_sword_and_shield_roll_right[] =
{
    "../assets/character/character_with_sword_and_shield/roll/roll right1.png",
    "../assets/character/character_with_sword_and_shield/roll/roll right2.png",
    "../assets/character/character_with_sword_and_shield/roll/roll right3.png",
    "../assets/character/character_with_sword_and_shield/roll/roll right4.png",
};
#define ANIM_ID_30 paths_char_with_sword_and_shield_roll_right

static char *paths_char_with_sword_and_shield_roll_up[] =
{
    "../assets/character/character_with_sword_and_shield/roll/roll up1.png",
    "../assets/character/character_with_sword_and_shield/roll/roll up2.png",
    "../assets/character/character_with_sword_and_shield/roll/roll up3.png",
    "../assets/character/character_with_sword_and_shield/roll/roll up4.png",
};
#define ANIM_ID_31 paths_char_with_sword_and_shield_roll_up

static char *paths_char_with_pistol_idle_down[] =
{
    "../assets/character/character_with_pistol/idle/idle down1.png",
    "../assets/character/character_with_pistol/idle/idle down2.png",
    "../assets/character/character_with_pistol/idle/idle down3.png",
    "../assets/character/character_with_pistol/idle/idle down4.png",
};
#define ANIM_ID_32 paths_char_with_pistol_idle_down

static char *paths_char_with_pistol_idle_left[] =
{
    "../assets/character/character_with_pistol/idle/idle left1.png",
    "../assets/character/character_with_pistol/idle/idle left2.png",
    "../assets/character/character_with_pistol/idle/idle left3.png",
    "../assets/character/character_with_pistol/idle/idle left4.png",
};
#define ANIM_ID_33 paths_char_with_pistol_idle_left

static char *paths_char_with_pistol_idle_right[] =
{
    "../assets/character/character_with_pistol/idle/idle right1.png",
    "../assets/character/character_with_pistol/idle/idle right2.png",
    "../assets/character/character_with_pistol/idle/idle right3.png",
    "../assets/character/character_with_pistol/idle/idle right4.png",
};
#define ANIM_ID_34 paths_char_with_pistol_idle_right

static char *paths_char_with_pistol_idle_up[] =
{
    "../assets/character/character_with_pistol/idle/idle up1.png",
    "../assets/character/character_with_pistol/idle/idle up2.png",
    "../assets/character/character_with_pistol/idle/idle up3.png",
    "../assets/character/character_with_pistol/idle/idle up4.png",
};
#define ANIM_ID_35 paths_char_with_pistol_idle_up

static char *paths_char_with_pistol_walk_down[] =
{
    "../assets/character/character_with_pistol/walk/walk down1.png",
    "../assets/character/character_with_pistol/walk/walk down2.png",
    "../assets/character/character_with_pistol/walk/walk down3.png",
    "../assets/character/character_with_pistol/walk/walk down4.png",
};
#define ANIM_ID_36 paths_char_with_pistol_walk_down

static char *paths_char_with_pistol_walk_left[] =
{
    "../assets/character/character_with_pistol/walk/walk left1.png",
    "../assets/character/character_with_pistol/walk/walk left2.png",
    "../assets/character/character_with_pistol/walk/walk left3.png",
    "../assets/character/character_with_pistol/walk/walk left4.png",
};
#define ANIM_ID_37 paths_char_with_pistol_walk_left

static char *paths_char_with_pistol_walk_right[] =
{
    "../assets/character/character_with_pistol/walk/walk right1.png",
    "../assets/character/character_with_pistol/walk/walk right2.png",
    "../assets/character/character_with_pistol/walk/walk right3.png",
    "../assets/character/character_with_pistol/walk/walk right4.png",
};
#define ANIM_ID_38 paths_char_with_pistol_walk_right

static char *paths_char_with_pistol_walk_up[] =
{
    "../assets/character/character_with_pistol/walk/walk up1.png",
    "../assets/character/character_with_pistol/walk/walk up2.png",
    "../assets/character/character_with_pistol/walk/walk up3.png",
    "../assets/character/character_with_pistol/walk/walk up4.png",
};
#define ANIM_ID_39 paths_char_with_pistol_walk_up

static char *paths_char_with_pistol_attack_down[] =
{
    "../assets/character/character_with_pistol/attack/shoot down1.png",
    "../assets/character/character_with_pistol/attack/shoot down2.png",
    "../assets/character/character_with_pistol/attack/shoot down3.png",
    "../assets/character/character_with_pistol/attack/shoot down4.png",
};
#define ANIM_ID_40 paths_char_with_pistol_attack_down

static char *paths_char_with_pistol_attack_left[] =
{
    "../assets/character/character_with_pistol/attack/shoot left1.png",
    "../assets/character/character_with_pistol/attack/shoot left2.png",
    "../assets/character/character_with_pistol/attack/shoot left3.png",
    "../assets/character/character_with_pistol/attack/shoot left4.png",
};
#define ANIM_ID_41 paths_char_with_pistol_attack_left

static char *paths_char_with_pistol_attack_right[] =
{
    "../assets/character/character_with_pistol/attack/shoot right1.png",
    "../assets/character/character_with_pistol/attack/shoot right2.png",
    "../assets/character/character_with_pistol/attack/shoot right3.png",
    "../assets/character/character_with_pistol/attack/shoot right4.png",
};
#define ANIM_ID_42 paths_char_with_pistol_attack_right

static char *paths_char_with_pistol_attack_up[] =
{
    "../assets/character/character_with_pistol/attack/shoot up1.png",
    "../assets/character/character_with_pistol/attack/shoot up2.png",
    "../assets/character/character_with_pistol/attack/shoot up3.png",
    "../assets/character/character_with_pistol/attack/shoot up4.png",
};
#define ANIM_ID_43 paths_char_with_pistol_attack_up

static char *paths_char_with_pistol_roll_down[] =
{
    "../assets/character/character_with_pistol/roll/roll down1.png",
    "../assets/character/character_with_pistol/roll/roll down2.png",
    "../assets/character/character_with_pistol/roll/roll down3.png",
    "../assets/character/character_with_pistol/roll/roll down4.png",
};
#define ANIM_ID_44 paths_char_with_pistol_roll_down

static char *paths_char_with_pistol_roll_left[] =
{
    "../assets/character/character_with_pistol/roll/roll left1.png",
    "../assets/character/character_with_pistol/roll/roll left2.png",
    "../assets/character/character_with_pistol/roll/roll left3.png",
    "../assets/character/character_with_pistol/roll/roll left4.png",
};
#define ANIM_ID_45 paths_char_with_pistol_roll_left

static char *paths_char_with_pistol_roll_right[] =
{
    "../assets/character/character_with_pistol/roll/roll right1.png",
    "../assets/character/character_with_pistol/roll/roll right2.png",
    "../assets/character/character_with_pistol/roll/roll right3.png",
    "../assets/character/character_with_pistol/roll/roll right4.png",
};
#define ANIM_ID_46 paths_char_with_pistol_roll_right

static char *paths_char_with_pistol_roll_up[] =
{
    "../assets/character/character_with_pistol/roll/roll up1.png",
    "../assets/character/character_with_pistol/roll/roll up2.png",
    "../assets/character/character_with_pistol/roll/roll up3.png",
    "../assets/character/character_with_pistol/roll/roll up4.png",
};
#define ANIM_ID_47 paths_char_with_pistol_roll_up


static char *paths_char_dead[] =
{
    "../assets/character/death_animation/death1.png",
    "../assets/character/death_animation/death2.png",
    "../assets/character/death_animation/death3.png",
    "../assets/character/death_animation/death4.png",
};
#define ANIM_ID_100 paths_char_dead

typedef struct
{
    f32 speed;
    u32 required_loop_count;
    u32 frame_count;
    char **paths;
} kvase_AnimAsset;

static kvase_AnimAsset anim_asset_0 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_0), .paths = ANIM_ID_0};
static kvase_AnimAsset anim_asset_1 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_1), .paths = ANIM_ID_1};
static kvase_AnimAsset anim_asset_2 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_2), .paths = ANIM_ID_2};
static kvase_AnimAsset anim_asset_3 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_3), .paths = ANIM_ID_3};
static kvase_AnimAsset anim_asset_4 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_4), .paths = ANIM_ID_4};
static kvase_AnimAsset anim_asset_5 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_5), .paths = ANIM_ID_5};
static kvase_AnimAsset anim_asset_6 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_6), .paths = ANIM_ID_6};
static kvase_AnimAsset anim_asset_7 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_7), .paths = ANIM_ID_7};
static kvase_AnimAsset anim_asset_8 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_8), .paths = ANIM_ID_8};
static kvase_AnimAsset anim_asset_9 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_9), .paths = ANIM_ID_9};

static kvase_AnimAsset anim_asset_10 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_10), .paths = ANIM_ID_10};
static kvase_AnimAsset anim_asset_11 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_11), .paths = ANIM_ID_11};
static kvase_AnimAsset anim_asset_12 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_12), .paths = ANIM_ID_12};
static kvase_AnimAsset anim_asset_13 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_13), .paths = ANIM_ID_13};
static kvase_AnimAsset anim_asset_14 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_14), .paths = ANIM_ID_14};
static kvase_AnimAsset anim_asset_15 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_15), .paths = ANIM_ID_15};
static kvase_AnimAsset anim_asset_16 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_16), .paths = ANIM_ID_16};
static kvase_AnimAsset anim_asset_17 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_17), .paths = ANIM_ID_17};
static kvase_AnimAsset anim_asset_18 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_18), .paths = ANIM_ID_18};
static kvase_AnimAsset anim_asset_19 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_19), .paths = ANIM_ID_19};

static kvase_AnimAsset anim_asset_20 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_20), .paths = ANIM_ID_20};
static kvase_AnimAsset anim_asset_21 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_21), .paths = ANIM_ID_21};
static kvase_AnimAsset anim_asset_22 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_22), .paths = ANIM_ID_22};
static kvase_AnimAsset anim_asset_23 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_23), .paths = ANIM_ID_23};
static kvase_AnimAsset anim_asset_24 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_24), .paths = ANIM_ID_24};
static kvase_AnimAsset anim_asset_25 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_25), .paths = ANIM_ID_25};
static kvase_AnimAsset anim_asset_26 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_26), .paths = ANIM_ID_26};
static kvase_AnimAsset anim_asset_27 = {.speed = 0.05f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_27), .paths = ANIM_ID_27};
static kvase_AnimAsset anim_asset_28 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_28), .paths = ANIM_ID_28};
static kvase_AnimAsset anim_asset_29 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_29), .paths = ANIM_ID_29};

static kvase_AnimAsset anim_asset_30 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_30), .paths = ANIM_ID_30};
static kvase_AnimAsset anim_asset_31 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_31), .paths = ANIM_ID_31};
static kvase_AnimAsset anim_asset_32 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_32), .paths = ANIM_ID_32};
static kvase_AnimAsset anim_asset_33 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_33), .paths = ANIM_ID_33};
static kvase_AnimAsset anim_asset_34 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_34), .paths = ANIM_ID_34};
static kvase_AnimAsset anim_asset_35 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_35), .paths = ANIM_ID_35};
static kvase_AnimAsset anim_asset_36 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_36), .paths = ANIM_ID_36};
static kvase_AnimAsset anim_asset_37 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_37), .paths = ANIM_ID_37};
static kvase_AnimAsset anim_asset_38 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_38), .paths = ANIM_ID_38};
static kvase_AnimAsset anim_asset_39 = {.speed = 0.1f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_39), .paths = ANIM_ID_39};

static kvase_AnimAsset anim_asset_40 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_40), .paths = ANIM_ID_40};
static kvase_AnimAsset anim_asset_41 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_41), .paths = ANIM_ID_41};
static kvase_AnimAsset anim_asset_42 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_42), .paths = ANIM_ID_42};
static kvase_AnimAsset anim_asset_43 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_43), .paths = ANIM_ID_43};
static kvase_AnimAsset anim_asset_44 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_44), .paths = ANIM_ID_44};
static kvase_AnimAsset anim_asset_45 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_45), .paths = ANIM_ID_45};
static kvase_AnimAsset anim_asset_46 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_46), .paths = ANIM_ID_46};
static kvase_AnimAsset anim_asset_47 = {.speed = 0.1f, .required_loop_count = 1, .frame_count = array_count(ANIM_ID_47), .paths = ANIM_ID_47};

static kvase_AnimAsset anim_asset_100 = {.speed = 0.4f, .required_loop_count = 0, .frame_count = array_count(ANIM_ID_100), .paths = ANIM_ID_100};
#endif // KVASE_ASSET_PATHS_H
